---
title: Cheat Sheets
description: We're compiling a library of useful database books from around the Internet to help you on your software journey!
position: 5003
category: Databases
---


## SQL

Structured Query Language (SQL) is a standardized programming language that is used to manage relational databases and perform various operations on the data in them.

### MySQL Cheatsheet

[![MySQL Cheatsheet Preview](https://i.imgur.com/ze2ICnl.png)](https://s3-us-west-2.amazonaws.com/dbshostedfiles/dbs/sql_cheat_sheet_mysql.pdf)

[Source: databasestar.com ](https://www.databasestar.com/)

<cta-button text="View" link="https://s3-us-west-2.amazonaws.com/dbshostedfiles/dbs/sql_cheat_sheet_mysql.pdf"></cta-button>


### PostgreSQL Cheatsheet 

[![PostgreSQL Cheatsheet Preview](https://i.imgur.com/GOJo3PT.png)](https://s3-us-west-2.amazonaws.com/dbshostedfiles/dbs/sql_cheat_sheet_pgsql.pdf)

[Source: databasestar.com ](https://www.databasestar.com/)

<cta-button text="View" link="https://s3-us-west-2.amazonaws.com/dbshostedfiles/dbs/sql_cheat_sheet_pgsql.pdf"></cta-button>


### SQL Cheat Sheet

[![SQL Cheat Sheet Preview](https://i.imgur.com/WxaJdwz.png)](https://www.dataquest.io/wp-content/uploads/2021/01/dataquest-sql-cheat-sheet.pdf)

[Source: dataquest.io](https://www.dataquest.io/)

<cta-button text="View" link="https://www.dataquest.io/wp-content/uploads/2021/01/dataquest-sql-cheat-sheet.pdf"></cta-button>


### SQL cheat sheet

[![SQL Cheat Sheet Preview](https://i.imgur.com/JeqCZs0.png)](https://www.sqltutorial.org/sql-cheat-sheet/)

[Source: sqltutorial.org](https://www.sqltutorial.org/)

<cta-button text="View" link="https://www.sqltutorial.org/sql-cheat-sheet/"></cta-button>


### SQL Basics Cheat Sheet

[![SQL Cheat Sheet Preview](https://i.imgur.com/kN3t2U1.png)](https://learnsql.com/blog/sql-basics-cheat-sheet/sql-basics-cheat-sheet-a4.pdf)

[Source: learnsql.com](https://learnsql.com/)

<cta-button text="View" link="https://learnsql.com/blog/sql-basics-cheat-sheet/sql-basics-cheat-sheet-a4.pdf"></cta-button>


### SQL Commands Cheat sheet

[![SQL Commands Cheat sheet Preview](https://i.imgur.com/W0FRoEj.png)](https://intellipaat.com/mediaFiles/2019/02/SQL-Commands-Cheat-Sheet.pdf)

[Source: intellipaat.com](https://intellipaat.com/)

<cta-button text="View" link="https://intellipaat.com/mediaFiles/2019/02/SQL-Commands-Cheat-Sheet.pdf"></cta-button>


</br>

<cta-button text="More Cheatsheets On SQL" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#sql"></cta-button>


## MongoDB

MongoDB is a document-oriented NoSQL database used for high volume data storage. Instead of using tables and rows as in the traditional relational databases, MongoDB makes use of collections and documents. Documents consist of key-value pairs which are the basic unit of data in MongoDB. Collections contain sets of documents and function which is the equivalent of relational database tables.

### MongoDB Cheat Sheet - MongoDB (HTML)

[![MongoDB Cheat Sheet Preview ](https://i.imgur.com/bPHDc37.png)](https://www.mongodb.com/developer/quickstart/cheat-sheet/)

<cta-button text="View" link="https://www.mongodb.com/developer/quickstart/cheat-sheet/"></cta-button>

### MongoDB Cheat Sheet - codecentric (PDF)

[![MongoDB Cheat Sheet Preview](https://i.imgur.com/fCOjAYb.png)](https://blog.codecentric.de/files/2012/12/MongoDB-CheatSheet-v1_0.pdf)

<cta-button text="View" link="https://blog.codecentric.de/files/2012/12/MongoDB-CheatSheet-v1_0.pdf"></cta-button>

### Quick Cheat Sheet for Mongo DB Shell commands 

[![Quick Cheat Sheet for Mongo DB Shell commands Preview](https://i.imgur.com/kW3vsvA.png)](https://gist.github.com/michaeltreat/d3bdc989b54cff969df86484e091fd0c)

<cta-button text="View" link="https://gist.github.com/michaeltreat/d3bdc989b54cff969df86484e091fd0c"></cta-button>

</br>

<cta-button text="More Cheatsheets On MongoDB" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#mongodb"></cta-button>