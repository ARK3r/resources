---
title: Courses
description: We're compiling useful robotics' courses from top universities to help you on your software journey!
position: 11000
category: Robotics
---

## Introduction to Robotics (Stanford)

This is an introduction course to robotics which covers topics such as Spatial Descriptions, Forward Kinematics, Inverse Kinematics, Jacobians, Dynamics, Motion Planning and Trajectory Generation, Position and Force Control, and Manipulator Design.

<youtube-video id="0yD3uBshJB0?list=PL65CC0384A1798ADF"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/watch?list=PL65CC0384A1798ADF&v=0yD3uBshJB0"></cta-button>


## Underactuated Robotics (MIT)

Robots today move far too conservatively, using control systems that attempt to maintain full control authority at all times. Humans and animals move much more aggressively by routinely executing motions which involve a loss of instantaneous control authority. Controlling nonlinear systems without complete control authority requires methods that can reason about and exploit the natural dynamics of our machines.

This course discusses nonlinear dynamics and control of underactuated mechanical systems, with an emphasis on machine learning methods. Topics include nonlinear dynamics of passive robots (walkers, swimmers, flyers), motion planning, partial feedback linearization, energy-shaping control, analytical optimal control, reinforcement learning/approximate optimal control, and the influence of mechanical design on control. Discussions include examples from biology and applications to legged locomotion, compliant manipulation, underwater robots, and flying machines.


<youtube-video id="Z8oMbOj9IWM?list=PL58F1D0056F04CF8C"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PL58F1D0056F04CF8C"></cta-button>
