---
title: Introduction
description: We're compiling a library of useful Project Management videos from around the Internet to help you on your software journey!
position: 4000
category: Product Management
---

## What Is Product Management And How To Start?

In this video you will learn what product management is and why is it a growing role in the marketplace.

You'll watch Justin Ziccardi talk about Product Management, a rapidly evolving discipline that, according to Hired.com was the highest paid tech role of 2018.

<youtube-video id="YIz774Gjo1E?list=PLzKJi2GjpkEELRD-YatYYLyItQ9O_MPU8"></youtube-video>






<iframe width="560" height="315" src="https://www.youtube.com/embed/YIz774Gjo1E?list=PLzKJi2GjpkEELRD-YatYYLyItQ9O_MPU8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Product Management 

A detailed guide to learn about product management.

<cta-button text="Guide" link="https://github.com/ProductHired/open-product-management"></cta-button>

A curated list of awesome resources for product/program managers to learn and grow.

<cta-button text="Resources" link="https://github.com/dend/awesome-product-management"></cta-button>

## Project Management - The Futur

This course will help you learn how to:

- How to Break Down Projects?
- How to Manage Your Clients?
- Tips to - Get Your Work Done on Time.
- How To Give Feedback To Teams That Empower & Engage Creativity?
- How To Talk To Clients That Use Abstract & Unclear Language?
  And much more.

  <youtube-video id="zlnVc1nBTto?list=PLzKJi2GjpkEEFRTKkCMIlPxCk8UuzQHU4"> </youtube-video>

<cta-button  link="https://www.youtube.com/playlist?list=PLzKJi2GjpkEEFRTKkCMIlPxCk8UuzQHU4" text="View Playlist!" > </cta-button>

## Kanban VS Scrum

In this video, you will learn about two popular product management frameworks "Kanban" and "Scrum", their pros and cons, and the flexibility they possess.

<youtube-video id="zSVB8kh9rqs"> </youtube-video>
