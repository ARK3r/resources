---
title: Concepts
description: We're compiling a library of useful Operating Systems concepts from around the Internet to help you on your software journey!
position: 16500
category: Operating Systems
---

## Operating System Overview 

[Source : ahmaazouzi from Github](https://github.com/ahmaazouzi)

An operating system:
- Makes it easy to use programs.
- Allows multiple programs to run simultaneously without clashes. They share the same memory and their data doesn't get scrambled together.
- Facilitates communication between the computer and devices attached to it.
- Gives the user an easy interface for interacting with the device's hardware.

[![Operating System Overview - tutorialspoint.com](https://i.imgur.com/6ixlWD0.png)](https://github.com/ahmaazouzi/cs-topics/blob/master/os/overview.md)

**Image source: tutorialspoint.com**

<cta-button text="Learn More" link="https://github.com/ahmaazouzi/cs-topics/blob/master/os/overview.md"></cta-button>


## Processes 

[Source : ahmaazouzi from Github](https://github.com/ahmaazouzi)

Processes are probably the most basic and powerful abstraction provided by operating systems.
In this part, you will learn: 
- What is a process and how an OS uses it.
- Important UNIX system calls, designed specifically for creating and managing processes.
- Direct execution, and how the OS uses it to create efficient processes that it can still control and supervise.

[![Process States in Operating System - Webeduclick.com](https://i.imgur.com/lYxRKtY.png)](https://github.com/ahmaazouzi/cs-topics/blob/master/os/processes.md)

**Image source: Webeduclick.com**

<cta-button text="Learn More" link="https://github.com/ahmaazouzi/cs-topics/blob/master/os/processes.md"></cta-button>


## Scheduling 

[Source : ahmaazouzi from Github](https://github.com/ahmaazouzi)

Scheduling basically refers to the high-level policies used by the OS to manage slices of time it allots to each process.

[![OS scheduling Preview - krivalar.com](https://i.imgur.com/kWuXkrq.png)](https://github.com/ahmaazouzi/cs-topics/blob/master/os/scheduling.md)

**Image source: krivalar.com**

<cta-button text="Learn More" link="https://github.com/ahmaazouzi/cs-topics/blob/master/os/scheduling.md"></cta-button>