---
title: Books
description: We're compiling a library of useful Operating Systems books from around the Internet to help you on your software journey!
position: 16501
category: Operating Systems
---

## Operating Systems

### A short introduction to operating systems 

<book-cover link="http://markburgess.org/os/os.pdf" img-src="https://i.imgur.com/zsaXQy5.jpg" alt="Book cover for A short introduction to operating systems"> </book-cover>

*A Short Introduction to Operating Systems* by Mark Burgess book is a compilation of lecture notes on operating systems printed at the University of College Oslo, Norway.

It is a good combination of programming examples along with theory. It is recommended for readers who are interested to get incite on the roles of an operating system and how operating systems work.

<cta-button text="Complete Book" link="http://markburgess.org/os/os.pdf"></cta-button>


### Linux From Scratch 

<book-cover link="https://www.linuxfromscratch.org/lfs/view/stable/" img-src="https://i.imgur.com/5UpTgpH.jpg" alt="Book cover for Linux From Scratch "> </book-cover>

The goal of Linux From Scratch is to build a complete and usable foundation-level system.  

This book will help you learn how a Linux system works from the inside out. Building an LFS system helps demonstrate what makes Linux tick, and how things work together and depend on each other. One of the best things that this learning experience can provide is the ability to customize a Linux system to suit your own unique needs.

<cta-button text="Complete Book" link="https://www.linuxfromscratch.org/lfs/view/stable/"></cta-button>


### The Art of Unix Programming 
<book-cover link="http://catb.org/esr/writings/taoup/html/" img-src="https://i.imgur.com/DCc1ylN.jpg" alt="Book cover for The Art of Unix Programming "> </book-cover>

*The Art of UNIX Programming* poses the belief that understanding the unwritten UNIX engineering tradition and mastering its design patterns will help programmers of all stripes to become better programmers.

This book attempts to capture the engineering wisdom and design philosophy of the UNIX, Linux, and Open Source software development community as it has evolved over the past three decades, and as it is applied today by the most experienced programmers. Eric Raymond offers the next generation of "hackers" the unique opportunity to learn the connection between UNIX philosophy and practice through careful case studies of the very best UNIX/Linux programs.

<cta-button text="Complete Book" link="http://catb.org/esr/writings/taoup/html/"></cta-button>


### Project Oberon: The Design of an Operating System, a Compiler, and a Computer 

<book-cover link="http://people.inf.ethz.ch/wirth/ProjectOberon/index.html" img-src="https://i.imgur.com/bOCFIyb.jpg" alt="Book cover for "> </book-cover>

This book presents the results of Project Oberon, namely an entire software environment for a modern workstation. The project was undertaken by the authors in the years 1986-89, and its primary goal was to design and implement an entire system from scratch, and to structure it in such a way that it can be described, explained, and understood as a whole.

In order to become confronted with all aspects, problems, design decisions and details, the authors not only conceived but also programmed the entire system described in this book, and more. The book gives advice on how a system might be built, and demonstrates how one was built. Program listings therefore play a key role in this text, because they alone contain the ultimate explanations.

<cta-button text="Complete Book" link="http://people.inf.ethz.ch/wirth/ProjectOberon/index.html"></cta-button>

</br>

<cta-button text="More Books On Operating Systems " link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#operating-systems"></cta-button>

