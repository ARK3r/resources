---
title: Cheat Sheats
description:  We're compiling a library of useful programming languages' cheat sheets from around the Internet to help you on your software journey!
position: 12506
category: Programming
---


## APL

- [A reference card for GNU APL - jpellegrini (PDF)](https://github.com/jpellegrini/gnu-apl-refcard/blob/master/aplcard.pdf)

- [Cheat Sheets - Dyalog (PDF)](https://www.dyalog.com/documentation_182.htm#CHEAT)

- [Dyalog APL - Vocabulary - awagga (HTML)](https://awagga.github.io/dyalog/voc/)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#apl"></cta-button>


## Bash

- [Bash Cheatsheet - CheatSheet.Wtf - smokingcuke (HTML)](https://www.cheatsheet.wtf/bash/)

- [Bash Scripting cheatsheet Devhints (HTML)](https://devhints.io/bash)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#bash"></cta-button>


## C#

- [C# Cheat Sheet - Simple Cheat Sheet (HTML)](https://simplecheatsheet.com/tag/c-cheat-sheet-1/)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#csharp"></cta-button>


## C++

- [C++ Cheatsheet - CodeWithHarry (HTML)](https://www.codewithharry.com/blogpost/cpp-cheatsheet)

- [C++ Quick Reference - Hooman Baradaran (PDF)](http://www.hoomanb.com/cs/quickref/CppQuickRef.pdf)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#cpp"></cta-button>

## C

- [C Reference Card (ANSI) (PDF)](https://users.ece.utexas.edu/~adnan/c-refcard.pdf)

- [Systems Programming Cheat Sheet](https://github.com/jstrieb/systems-programming-cheat-sheet)

- [The C Cheat Sheet: An Introduction to Programming in C - Andrew Sterian (PDF)](https://sites.ualberta.ca/~ygu/courses/geoph624/codes/C.CheatSheet.pdf)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#c"></cta-button>


## Clojure

- [Clojure Cheatsheet](https://clojure.org/api/cheatsheet)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#clojure"></cta-button>


## Data Science

- [Cheatsheets for Data Scientists - Datacamp (PDF)](https://www.datacamp.com/community/data-science-cheatsheets)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#data-science"></cta-button>


## Docker

- [Docker Cheat Sheet - Anthony Rioux, Low Orbit Flux (HTML, PDF)](https://low-orbit.net/docker-cheat-sheet)

- [Docker Security Cheat Sheet - OWASP Cheat Sheet Series](https://cheatsheetseries.owasp.org/cheatsheets/Docker_Security_Cheat_Sheet.html)

- [Docker Cheatsheet: Docker commands that developers should know - Vishnu Chilamakuru (HTML)](https://vishnuch.tech/docker-cheatsheet)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#docker"></cta-button>


## Git

- [Git Cheat Sheet - GitHub (PDF)](https://education.github.com/git-cheat-sheet-education.pdf)

- [Git Cheat Sheet - GitLab (PDF)](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)

- [GitHub Cheat Sheet - Tim Green (Markdown)](https://github.com/tiimgreen/github-cheat-sheet)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#git"></cta-button>


## Go

- [cht.sh Go Cheatsheet](https://cht.sh/go/:learn)

- [Go Cheatsheet - devhints, Rico Santa Cruz (HTML)](https://devhints.io/gohttps://devhints.io/go)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#go"></cta-button>


## HTML and CSS

- [HTML & CSS Emmet Cheat Sheet - Emmet Documentation (HTML, PDF)](https://docs.emmet.io/cheat-sheet/)

- [CSS Flexbox Cheatsheet - Chris Coyier (HTML)](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

- [CSS Grid Cheatsheet - Chris House (HTML)](https://css-tricks.com/snippets/css/complete-guide-grid/)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#html--css"></cta-button>


## Java

- [Java - Moshfegh Hamedani (PDF)](https://programmingwithmosh.com/wp-content/uploads/2019/07/Java-Cheat-Sheet.pdf)

- [Java Cheatsheet - CodeWithHarry (HTML)](https://www.codewithharry.com/blogpost/java-cheatsheet)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#java"></cta-button>


## Javascript 

- [JavaScript Cheatsheet - Codecademy (HTML)](https://www.codecademy.com/learn/introduction-to-javascript/modules/learn-javascript-introduction/cheatsheet)

- [JavaScript CheatSheet (HTML)](https://htmlcheatsheet.com/js/)

- [JavaScript Regex Cheatsheet - Debuggex (HTML)](https://www.debuggex.com/cheatsheet/regex/javascript)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#javascript"></cta-button>


### jQuery 

- [jQuery CheatSheet (HTML)](https://htmlcheatsheet.com/jquery/)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#jquery"></cta-button>


### Nest.js

- [Nest.js CheatSheet (GitHub Gist)](https://gist.github.com/guiliredu/0aa9e4d338bbeeac369a597e87c9ba46)

<cta-button text="More cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#nestjs"></cta-button>


### Nuxt.js

- [Nuxt.js Essentials Cheatsheet - Vue Mastery (PDF)](https://www.vuemastery.com/pdf/Nuxtjs-Cheat-Sheet.pdf)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#nuxtjs"></cta-button>


### React.js

- [React Cheatsheet - Codecademy (HTML)](https://www.codecademy.com/learn/react-101/modules/react-101-jsx-u/cheatsheet)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#reactjs"></cta-button>


### Vue.js

- [Vue Essential Cheatsheet - Vue Mastery (PDF)](https://www.vuemastery.com/pdf/Vue-Essentials-Cheat-Sheet.pdf)

- [Vue.js 2.3 Complete API Cheat Sheet - Marcos Neves, Marozed (HTML)](https://marozed.com/vue-cheatsheet)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#vuejs"></cta-button>


## Kotlin

- [Kotlin Cheatsheet and Quick Reference - Ray Wenderlich (PDF)](https://koenig-media.raywenderlich.com/uploads/2019/11/RW-Kotlin-Cheatsheet-1.1.pdf)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#kotlin"></cta-button>


## Kubernetes

- [Handy Cheat Sheet for Kubernetes Beginners - Kubernetes Documentation: kubectl Cheat Sheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#kubernetes"></cta-button>


## Language Translations

- [Swift and C# Quick Reference - Language Equivalents and Code Examples - Globalnerdy (PDF)](https://www.globalnerdy.com/wordpress/wp-content/uploads/2015/03/SwiftCSharpPoster.pdf)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#language-translations"></cta-button>


## Markdown

- [Markdown Cheat Sheet - Markdown Guide (HTML)](https://www.markdownguide.org/cheat-sheet/)

- [Markdown Here - Adam Pritchard](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#markdown"></cta-button>


## MATLAB

- [MATLAB Basic Functions Reference Sheet - MathWorks (PDF)](https://www.mathworks.com/content/dam/mathworks/fact-sheet/matlab-basic-functions-reference.pdf)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#matlab"></cta-button>


## MongoDB

- [MongoDB Cheat Sheet - MongoDB (HTML)](https://www.mongodb.com/developer/quickstart/cheat-sheet/)

- [MongoDB Cheat Sheet - codecentric (PDF)](https://blog.codecentric.de/files/2012/12/MongoDB-CheatSheet-v1_0.pdf)

- [Quick Cheat Sheet for Mongo DB Shell commands - Michael Treat's Quick Cheat Sheet](https://gist.github.com/michaeltreat/d3bdc989b54cff969df86484e091fd0c)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#mongodb"></cta-button>


## Perl

- [Perl Reference card (PDF)](https://michaelgoerz.net/refcards/perl_refcard.pdf)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#perl"></cta-button>


## PyTorch

- [PyTorch Framework Cheat Sheet - Simon Wenkel (PDF)](https://www.simonwenkel.com/publications/cheatsheets/pdf/cheatsheet_pytorch.pdf)

- [PyTorch Official Cheat Sheet - PyTorch (HTML)](https://pytorch.org/tutorials/beginner/ptcheat.html)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#pytorch"></cta-button>


## PHP

- [PHP Cheat Sheet - Nick Schäferhoff, WebsiteSetup (HTML) -- ](https://websitesetup.org/php-cheat-sheet/)[(PDF)](https://websitesetup.org/wp-content/uploads/2020/09/PHP-Cheat-Sheet.pdf)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#php"></cta-button>


## Python

- [Python Cheatsheet for beginners - Codeacademy (HTML)](https://www.codecademy.com/learn/learn-python-3)

- [Comprehensive Python Cheatsheet - Jure Šorn (HTML)](https://gto76.github.io/python-cheatsheet/)

- [Learn Python in Y minutes - LearnXinYMinutes (HTML)](https://learnxinyminutes.com/docs/python/)

- [Official Matplotlib cheat sheets - Matplotlib.org (LaTeX, PDF)](https://github.com/matplotlib/cheatsheets)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#python"></cta-button>


## R

- [All RStudio cheatsheets resources - RStudio.com (HTML site with PDF links)](https://www.rstudio.com/resources/cheatsheets/)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#r"></cta-button>


## Raspberry Pi

- [Basic GPIO layout configuration cheatsheet - University of Cambridge Computer Laboratory Raspberry Pi Projects Cheatsheet (PDF)](https://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/robot/cheat_sheet/)

- [Other Raspberry Pi Commands cheatsheet - RPi starter Kit (PDF)](https://www.raspberrypistarterkits.com/wp-content/uploads/2018/01/raspberry-pi-commands-cheat-sheet.pdf)

- [Raspberry Pi Basics cheatsheet - Woolsey Workshop (PDF)](https://www.woolseyworkshop.com/wp-content/uploads/WoolseyWorkshop_Cheatsheet_RaspberryPiBasics_v1.4.pdf)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#raspberry-pi"></cta-button>


## Ruby

- [Ruby Cheat Sheet - CodeConquest.com (PDF)](https://www.codeconquest.com/wp-content/uploads/Ruby-Cheat-Sheet-by-CodeConquestDOTcom.pdf)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#ruby"></cta-button>


## Rust

- [Rust Language Cheat Sheet (HTML)](https://cheats.rs/)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#rust"></cta-button>


## Solidity

- [Solidity Cheat Sheet - IntelliPaat (PDF)](https://intellipaat.com/mediaFiles/2019/03/Solidity-Cheat-Sheet.pdf)

- [Solidity Cheatsheet and Best practices - Manoj Ramesh](https://manojpramesh.github.io/solidity-cheatsheet/)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#solidity"></cta-button>


## SQL

- [MySQL Cheatsheet - Database Star (PDF)](https://s3-us-west-2.amazonaws.com/dbshostedfiles/dbs/sql_cheat_sheet_mysql.pdf)

- [PostgreSQL Cheatsheet - Database Star (PDF)](https://s3-us-west-2.amazonaws.com/dbshostedfiles/dbs/sql_cheat_sheet_pgsql.pdf)

- [SQL Cheat Sheet - Dataquest.io (PDF)](https://www.dataquest.io/wp-content/uploads/2021/01/dataquest-sql-cheat-sheet.pdf)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#sql"></cta-button>


## Tensorflow 

- [TensorFlow Quick Reference Table - Secret data scientist (HTML)](https://web.archive.org/web/20200922212358/https://www.aicheatsheets.com/static/pdfs/tensorflow_v_2.0.pdf)

- [TensorFlow v2.0 Cheat Sheet - Altoros (PDF)](https://secretdatascientist.com/tensor-flow-cheat-sheet/)

<cta-button text="More Cheatsheets" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#tensorflow"></cta-button>