---
title: Books
description:  We're compiling a library of useful programming books from around the Internet to help you on your software journey!
position: 12505
category: Programming
---

<alpha-navbar link="https://www.google.com/"> A </alpha-navbar>

## Programming Paradigms

- [Flow based Programming - J Paul Morrison](https://jpaulm.github.io/fbp/index.html)

- [Introduction to Functional Programming - J. Harrison](https://www.cl.cam.ac.uk/teaching/Lectures/funprog-jrh-1996/)

- [Making Sense of Stream Processing - Martin Kleppmann (PDF)](https://assets.confluent.io/m/2a60fabedb2dfbb1/original/20190307-EB-Making_Sense_of_Stream_Processing_Confluent.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#programming-paradigms"></cta-button>


## Parallel Programming

- [Programming on Parallel Machines; GPU, Multicore, Clusters and More - Norm Matloff Kerridge (PDF) (email address requested, not required)](https://heather.cs.ucdavis.edu/parprocbook)

- [Is Parallel Programming Hard, And, If So, What Can You Do About It? - Paul E. McKenney](https://mirrors.edge.kernel.org/pub/linux/kernel/people/paulmck/perfbook/perfbook.html)

- [High Performance Computing - Charles Severance & Kevin Dowd (PDF, ePUB)](https://cnx.org/contents/bb821554-7f76-44b1-89e7-8a2a759d1347%405.2)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#programming-paradigms"></cta-button>


## Competitive Programming

- [Competitive Programming – A Complete Guide](https://www.geeksforgeeks.org/competitive-programming-a-complete-guide/)

- [Competitive Programmer's Handbook - Antti Laaksonen (PDF)](https://cses.fi/book/book.pdf)

- [Principles of Algorithmic Problem Solving - Johan Sannemo (PDF)](https://www.csc.kth.se/~jsannemo/slask/main.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#competitive-programming"></cta-button>


## Graphics Programming

- [3D Game Shaders For Beginners - David Lettier (Git) (HTML)](https://github.com/lettier/3d-game-shaders-for-beginners)

- [Blender 3D: Noob to Pro - Wikibooks](https://en.wikibooks.org/wiki/Blender_3D%3A_Noob_to_Pro)

- [Notes for a Computer Graphics Programming Course - Dr. Steve Cunningham (PDF)](https://www.cs.csustan.edu/~rsc/CS3600F00/Notes.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#graphics-programming"></cta-button>


## Regular Expressions

- [JavaScript RegExp - Sundeep Agarwal](https://learnbyexample.github.io/learn_js_regexp/)

- [Python re(gex)? - Sundeep Agarwal](https://learnbyexample.github.io/py_regular_expressions/)

- [Regular Expressions for Regular Folk - Shreyas Minocha](https://refrf.dev/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#regular-expressions"></cta-button>

