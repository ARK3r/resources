---
title: videos
description: We're compiling a library of useful computer programming languages videos from around the Internet to help you on your software journey!
position: 12500
category: Programming
---

## JavaScript

This video introduces JavaScript, perhaps the most important technology of the modern web.

- What is JavaScript?
- What are JavaScript's key features?
- What's the difference between web APIs used by browsers vs NodeJS?

<youtube-video id="09XmbByy6Sk"> </youtube-video>

## Typescript

TypeScript is a strict syntactical superset of JavaScript and adds optional static typing to the language.

TypeScript is used to develop JavaScript applications for both client-side and server-side execution (as with Node. js or Deno).

The goal of TypeScript is to help catch mistakes early through a type system and to make JavaScript development more efficient.

<youtube-video id="zQnBQ4tB3ZA"> </youtube-video>

## Python

Python is arguably the world's most popular programming language. It is easy to learn, yet suitable in professional software like web applications, data science, and server-side scripts.

<youtube-video id="x7X9w_GIm1s"> </youtube-video>

## Java

Java is a programming language famous for its ability to compile to platform-independent bytecode. It powers enterprise web apps, big data pipelines, and android mobile apps. 

<youtube-video id="l9AzO1FMgM8"> </youtube-video>

## Kotlin

Kotlin is a programming language designed as a modern alternative to Java. It supports functional patterns, coroutines, multi-platform compilation, and is now the recommended language for Android mobile development. 

<youtube-video id="xT8oP0wy-A0"> </youtube-video>

## PHP

PHP Hypertext Preprocessor is a scripting language for building dynamic websites on the server. It remains one of the most popular programming languages in the world, powering tools like Wordpress, Laravel, and Symfony.

<youtube-video id="a7_WFUlFS94"> </youtube-video>

## Dart 

Dart is high-productivity statically-typed programming language capable of targeting multiple platforms. It's used by Flutter to produce fast client apps with an awesome developer experience.

<youtube-video id="NrO0CJCbYLA"> </youtube-video>

## Rust

Rust is a memory-safe compiled programming language for building high-performance systems. It has the simplicity of high-level languages (Go, Python), but the control of low-level languages (C, Cpp).

<youtube-video id="5C_HPTJg5ek"> </youtube-video>

## GO

Go (not Golang) was developed at Google as a modern version of C for high-performance server-side applications. 

<youtube-video id="446E-r0rXHI"> </youtube-video>

