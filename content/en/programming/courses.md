---
title: Courses
description:  We're compiling a library of useful computer programming courses from around the Internet to help you on your software journey!
position: 12501
category: Programming
---

## Free Courses

### Introduction to Programming (Python for Everybody)

This course is for students who do not have any programming experience. If you've never written a for-loop, or don't know what a string is in programming, start here. This course is self-paced, allowing you to adjust the number of hours you spend per week to meet your needs.

<cta-button text="Full Course" link="https://www.py4e.com/lessons"></cta-button>

## University Courses

### Programming Abstractions in C++ (Stanford)

This course will acquaint you with the C++ programming language and introduce advanced programming techniques such as recursion, algorithm analysis, data abstraction, explore classic data structures and algorithms, and give you practice applying these tools to solving complex problems.

<youtube-video id="Ua-31ucGAZ0?list=PLoCMsyE1cvdWiqgyzwAz_uGLSHsuYZlMX"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PLoCMsyE1cvdWiqgyzwAz_uGLSHsuYZlMX"></cta-button>

### Programming Methodology (Stanford)

This is an Introduction to the engineering of computer applications emphasizing modern software engineering principles: object-oriented design, decomposition, encapsulation, abstraction, and testing. Uses the Java programming language. Emphasis is on good programming style and the built-in facilities of the Java language. 

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PL84A56BC7F4A1F852"></cta-button>


### Programming Paradigms (Stanford)

Programming Paradigms introduces several programming languages, including C, Assembly, C++, Concurrent Programming, Scheme, and Python.  The course aims to teach students how to write code for each of these individual languages and to understand the programming paradigms behind these languages.

<youtube-video id="Ps8jOj7diA0?list=PL9D558D49CA734A02"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PL9D558D49CA734A02"></cta-button>

### Programming for the Puzzled (MIT)

This course builds a bridge between the recreational world of algorithmic puzzles (puzzles that can be solved by algorithms) and the pragmatic world of computer programming, teaching students to program while solving puzzles. Python syntax and semantics required to understand the code are explained as needed for each puzzle.

<youtube-video id="14UlXIZzwE4?list=PLUl4u3cNGP62QumaaZtCCjkID-NgqrleA"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PLUl4u3cNGP62QumaaZtCCjkID-NgqrleA"></cta-button>



