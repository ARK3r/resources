---
title: TED Talks
description: We're compiling a library of useful TED Talks from around the Internet to help you on your software journey!
position: 2000
category: Videos
---

## Free software, free society

**Richard Stallman at TEDxGeneva 2014**

In this talk, the founder of Free Software movement Richard Stallman describes how nonfree programs give companies control of their users and what users can do in order to recover control over their computing.

<youtube-video id="Ag1AKIl_2GM"> </youtube-video>

## Civilization starter kit

**Marcin Jakubowski at TEDxKC 2011**

Marcin Jakubowski is open-sourcing a set of blueprints for 50 farming tools that can be built cheaply from scratch. Call it a "civilization starter kit," Marcin believes that by opening the means of production, we can achieve abundance for all.

<youtube-video id="S63Cy64p2lQ"> </youtube-video>

## How Open Hardware will Take Over the World

**Nathan Seidle at TEDxBoulder 2013**

Join Nathan as he guides you through the world of patents, how long it really takes copycat products to appear in the marketplace, and show you how sharing can truly build profitable, growing companies.

<youtube-video id="Rfu_MKgu2Ik"> </youtube-video>
