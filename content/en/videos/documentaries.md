---
title: Documentaries
description: We're compiling a library of useful documentaries from around the Internet to help you on your software journey!
position: 2001
category: Videos
---

## Vue.js: The Documentary

**Honeypot.io - February 2020**

What began as a side project of a Google developer now shares the JS leaderboard with Facebook's React and Google's Angular. With the help of Sarah Drasner, Taylor Otwell, Thorsten Lünborg and many others from the Vue.js community, Evan You tells the story of how he fought against the odds to bring Vuejs to life.

<youtube-video id="OrxmtDw4pVI"></youtube-video>

## The Internet's Own Boy: The Story of Aaron Swartz

**Brian Knappenberger - June 2014**

The Internet's Own Boy depicts the life of American computer programmer, writer, political organizer and Internet activist Aaron Swartz. It features interviews with his family and friends as well as the internet luminaries who worked with him. The film tells his story up to his eventual suicide after a legal battle, and explores the questions of access to information and civil liberties that drove his work. - **[Archive.org](https://archive.org/details/TheInternetsOwnBoyTheStoryOfAaronSwartz)**

<youtube-video id="3Q6Fzbgs_Lg"></youtube-video>


- https://www.youtube.com/watch?v=RFjIBM0TR7U
- https://www.youtube.com/watch?v=BE77h7dmoQU
- https://www.youtube.com/watch?v=318elIq37PE