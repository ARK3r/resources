---
title: Games
description: We're compiling games that help you learn CSS!
position: 3502
category: CSS
---

## CSS Diner

A fun game to help you learn and practice CSS selectors.

<cta-button text="Play Now" link="https://flukeout.github.io/"></cta-button>

[![CSS Diner Preview](https://flukeout.github.io/images/fb-share.jpg)](https://flukeout.github.io/)

## Flexbox Froggy

A game that helps you learn Flexbox by having you help guide Froggy and friends to their desired lilypads using CSS code!

<cta-button text="Play Now" link="https://flexboxfroggy.com"></cta-button>

[![Flexbox Froggy Preview](https://flexboxfroggy.com/images/screenshot.png)](https://flexboxfroggy.com/)

## Grid Garden

A game that helps you learn CSS-Grid by having you write CSS code to grow your carrot garden!

<cta-button text="Play Now" link="https://cssgridgarden.com"></cta-button>

[![Grid Garden Preview](https://cssgridgarden.com/images/screenshot.png)](https://cssgridgarden.com/images/screenshot.png)
