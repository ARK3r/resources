---
title: Videos
description: We're We're curating videos that help you learn CSS!
position: 3500
category: CSS
---

## Intro to CSS

This video covers everything you need to know to get up and running with CSS in only 20 minutes. It covers CSS syntax, how to add CSS to your HTML, CSS colors, CSS units, the box model, and best practices for CSS walking through a full example of CSS being used to style an HTML page.

By the end of this video you will know enough about CSS to style any basic web pages in your own projects!

<youtube-video id="1PnVor36_40"> </youtube-video>

## CSS Flexbox

Flexbox is a flexible way to layout items as rows or columns. This video will help you learn the most important concepts of layout with CSS Flexbox.

<youtube-video id="K74l26pE4YA?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>

## CSS Grid

This video will help you learn the basics of CSS Grid.

Grid is a powerful tool that can build layouts in the context of columns and rows. It can dramatically simplify CSS layout code for responsive designs.

<youtube-video id="uuOXPWCh-6o?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>

## CSS Variables

In modern CSS, you can create CSS variables which contain values that can be reused throughout your stylesheets!

<youtube-video id="NtRmIp4eMjs?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>

## CSS Animation

Animation is the change from one CSS style to another over the dimension of time. This video will help you learn the basics of CSS transitions and keyframe animations.

<youtube-video id="HZHHBwzmJLk?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>

## CSS Pseudo-classes

You can use Pseudo-classes to style an element based on a change to its state. This video will help you learn how to use CSS pseudo-classes to style an element based on changes to its state. like :hover, :focus, and :nth-child.

<youtube-video id="kpXKwDGtjGE?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>

## CSS Pseudo-elements

This video wil help you learn how to use CSS pseudo-elements to style content that does not actually exist in your HTML, like ::before and ::after.

<youtube-video id="e1KpKBHJOrA?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>

## Tailwind

Tailwind is a utility-first CSS framework for building websites. It takes a functional approach to web design by providing thousands of tiny classes to use directly in your HTML.

<youtube-video id="mr15Xzb1Ook?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>

## Sass

SASS, sometimes referred to as SCSS, stands for syntactically awesome stylesheets.

This video will help you learn the basics of SASS, the most mature, stable, and powerful professional-grade CSS extension language in the world.

<youtube-video id="akDIJa0AP5c?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>

## PostCSS

PostCSS is a tool that allows you to use modern CSS features like nesting and mixins, while supporting legacy browsers.

This video will help you learn why this simple JavaScript library is one of the most popular tools in the web development industry. 

<youtube-video id="WhCXiEwdU1A"> </youtube-video>
