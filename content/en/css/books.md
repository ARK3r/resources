---
title: CSS Books
description: We're compiling CSS books that help you learn CSS!
position: 3503
category: CSS
---

## How to Code in HTML5 and CSS3 

<book-cover link="https://web.archive.org/web/20180816174417/http://howtocodeinhtml.com/HowToCodeInHTML5AndCSS3.pdf" img-src="https://i.imgur.com/moPlsq0.png" alt="Book cover for How to Code in HTML5 and CSS3 "> </book-cover>

[Author: D. Wielgosik](https://github.com/ferrante)

This book is highly recommended for absolute beginners. It doesn't require any experience in IT to start. The aim of this book is to show the art of making websites using plain language and practical analogies.

<cta-button text="Download PDF" link="https://web.archive.org/web/20180816174417/http://howtocodeinhtml.com/HowToCodeInHTML5AndCSS3.pdf"></cta-button>


## HTML5 Canvas 

<book-cover link="https://www.oreilly.com/library/view/html5-canvas/9781449308032/ch01.html" img-src="https://i.imgur.com/XECYzw8.jpg" alt="Book cover for HTML5 Canvas"> </book-cover>

[Author: S.Fulton and J.Fulton](https://intotheverticalblank.com/)

This book gets you started with the Canvas element, perhaps HTML5's most exciting feature. It will also help you learn how to build interactive multimedia applications using this element to draw, render text, manipulate images, and create animations.

<cta-button text="Complete Book" link="https://www.oreilly.com/library/view/html5-canvas/9781449308032/ch01.html"></cta-button>


## CSS Animation 101

<book-cover link="https://github.com/cssanimation/css-animation-101" img-src="https://i.imgur.com/CrDMlUo.png" alt="Book cover for CSS Animation 101"> </book-cover>


[Author: D. Hutchinson](http://hop.ie/)

This book gives you a solid introduction to the topic, combining theory with practical lessons. You’ll learn how, and why to use animation on your web pages and hopefully be inspired to try it on your own projects!

<cta-button text="Complete Book" link="https://github.com/cssanimation/css-animation-101"></cta-button>


## Adaptive Web Design

<book-cover link="https://adaptivewebdesign.info/1st-edition/" img-src="https://i.imgur.com/4Y4rKB5.png" alt="Book cover for Adaptive Web Design"> </book-cover>

[Author: A. Gustafson](https://www.aaron-gustafson.com/)

In this book, the author chronicles the origins of *progressive enhancement*, its philosophy, and mechanisms, and reveals the countless practical ways that you can apply progressive enhancement principles using HTML, CSS, and JavaScript.

<cta-button text="Latest Edition" link="https://adaptivewebdesign.info/2nd-edition/"></cta-button> <cta-button text="Free 1st Edition" link="https://adaptivewebdesign.info/1st-edition/"></cta-button>


## Maintainable CSS

<book-cover link="https://maintainablecss.com/" img-src="https://i.imgur.com/0Uxk1nA.png" alt="Book cover for MaintainableCSS"> </book-cover>

[Author: A. Silver](https://adamsilver.io/)

Write CSS without worrying that overzealous, pre-existing styles will cause problems. MaintainableCSS is an approach to writing modular, scalable and maintainable CSS.

<cta-button text="Complete Book" link="https://maintainablecss.com/"></cta-button>


## Web Visual Effects with CSS3 

<book-cover link="https://leanpub.com/web-visual-effects-with-css3/read" img-src="https://i.imgur.com/eVMXJ87.png" alt="Book cover for Web Visual Effects with CSS3"> </book-cover>

[Author: T. Mak](https://makzan.net/)

This is a book with examples and solutions to create practical visual effects on web by using CSS3.

<cta-button text="Complete Book" link="https://leanpub.com/web-visual-effects-with-css3/read"></cta-button>


## DOM Enlightenment 

<book-cover link="http://domenlightenment.com/" img-src="https://i.imgur.com/88oxueG.png" alt="Book cover for DOM Enlightenment "> </book-cover>

[Author: C. Lindley](http://www.codylindley.com/)

This book is about exploring the relationship between JavaScript and the modern HTML DOM.

<cta-button text="Complete Book" link="http://domenlightenment.com/"></cta-button>


## Pocket Guide to Writing SVG 

<book-cover link="https://svgpocketguide.com/" img-src="https://i.imgur.com/r86KUw0.jpg" alt="Book cover for Pocket Guide to Writing SVG"> </book-cover>

[Author: J. Trythall](https://jonitrythall.com/)

This guide is meant to provide a quick but thorough introduction to building SVG inline, and while it in no way covers all the available features, it should prove helpful in getting you started.


<cta-button text="Complete Book" link="https://svgpocketguide.com/"></cta-button>

</br>

<cta-button text="More Books On HTML / CSS" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#html--css"></cta-button>

<cta-button text="More Resources On HTML / CSS" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#html--css"></cta-button>

