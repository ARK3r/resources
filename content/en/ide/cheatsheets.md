---
title: Cheat sheets
description: We're compiling useful IDE cheat sheets around the internet to help you to help you on your software journey!
position: 8501
category: IDE
---

## Integrated Development Environment

### Visual Studio Code: Keyboard shortcuts for Windows 

[![Visual Studio Code: Keyboard shortcuts for Windows](https://i.imgur.com/c0ILpDS.png)](https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf)

<cta-button text="Download PDF" link="https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf"></cta-button>

### GNU Emacs Reference Card 

[![GNU Emacs Reference Card](https://i.imgur.com/mF61Xur.png)](https://www.gnu.org/software/emacs/refcards/pdf/refcard.pdf)

<cta-button text="Download PDF" link="https://www.gnu.org/software/emacs/refcards/pdf/refcard.pdf"></cta-button>

### Vim Cheat Sheet 

[![Vim Cheat Sheet](https://i.imgur.com/1xHCJ1k.png)](https://www.cs.cmu.edu/~15131/f17/topics/vim/vim-cheatsheet.pdf)

<cta-button text="Download PDF" link="https://www.cs.cmu.edu/~15131/f17/topics/vim/vim-cheatsheet.pdf"></cta-button>

</br>

<cta-button text="More Cheatsheets On IDE" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#visual-basic"></cta-button>

<cta-button text="More Resources On IDE" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#ide--editors"></cta-button>


