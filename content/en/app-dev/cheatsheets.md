---
title: Cheat Sheets
description: We're compiling a library of useful application development cheat sheets from around the internet to help you on your software journey!
position: 14002
category: Application Development
---

## Java

Java is one of the most popular programming languages in the world. With Java you can build various types of applications such as desktop, web, mobile apps and distributed systems.

### Java Cheatsheet - Moshfegh Hamedani 

[![Java Preview](https://i.imgur.com/N0NaEVV.png)](https://programmingwithmosh.com/wp-content/uploads/2019/07/Java-Cheat-Sheet.pdf)

<cta-button text="View" link="https://programmingwithmosh.com/wp-content/uploads/2019/07/Java-Cheat-Sheet.pdf"></cta-button>


### Java Cheatsheet - CodeWithHarry

[![Java Cheatsheet Preview](https://i.imgur.com/66ENot0.png)](https://www.codewithharry.com/blogpost/java-cheatsheet)

<cta-button text="View" link="https://www.codewithharry.com/blogpost/java-cheatsheet"></cta-button>

</br>

<cta-button text="More Cheatsheets On Java" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#java"></cta-button>

## Kotlin

Kotlin is a modern, trending programming language. It is easy to learn, especially if you already know Java (it is 100% compatible with Java). It is used to develop Android apps, server side apps, and much more.

### Kotlin Cheatsheet and Quick Reference 

[![TensorFlow Quick Reference Table Preview](https://i.imgur.com/Xo2Jggk.png)](https://koenig-media.raywenderlich.com/uploads/2019/11/RW-Kotlin-Cheatsheet-1.1.pdf)

<cta-button text="View" link="https://koenig-media.raywenderlich.com/uploads/2019/11/RW-Kotlin-Cheatsheet-1.1.pdf"></cta-button>

</br>

<cta-button text="More Cheatsheets On Kotlin" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#kotlin"></cta-button>
