---
title: Courses
description: We're compiling a library of useful Natural Language Processing university courses from around the Internet to help you on your software journey!
position: 14500
category: Natural Language Processing
---

## Introduction to Natural Language Processing

This course offered by [fast.ai](https://www.fast.ai/) teaches a blend of traditional NLP topics (including regex, SVD, naive bayes, tokenization) and recent neural network approaches (including RNNs, seq2seq, attention, and the transformer architecture), as well as addressing urgent ethical issues, such as bias and disinformation.

<cta-button text="Learn More" link="https://www.fast.ai/2019/07/08/fastai-nlp/"></cta-button>

## Natural Language Understanding (Stanford)

This course is focused on developing systems and algorithms for robust machine understanding of human language. 

It draws on theoretical concepts from linguistics, natural language processing, and machine learning.

It also contains special lectures on developing projects, presenting research results, and making connections with industry.

<youtube-video id="rha64cQRLs8?list=PLoROMvodv4rPt5D0zs3YhbWSZA8Q_DyiJ"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/watch?list=PLoROMvodv4rPt5D0zs3YhbWSZA8Q_DyiJ&v=rha64cQRLs8"></cta-button>


## Natural Language Processing | Dan Jurafsky at Stanford University

<youtube-video id="oWsMIW-5xUc?list=PLLssT5z_DsK8HbD2sPcUIDfQ7zmBarMYv"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PLLssT5z_DsK8HbD2sPcUIDfQ7zmBarMYv"></cta-button>



