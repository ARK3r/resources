---
title: Books
description: We're compiling useful web development books around the internet to help you understand web .
position: 2504
category: The Web
---


## Web Performance

- [Book of Speed - Stoyan Stefanov](https://www.bookofspeed.com/)

- [High Performance Browser Networking - Ilya Grigorik](https://hpbn.co/)

- [Mature Optimization - Carlos Bueno (PDF)](https://carlos.bueno.org/optimization/mature-optimization.pdf)


<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#web-performance"></cta-button>

## Web Services

- [RESTful Web Services (PDF)](http://restfulwebapis.org/RESTful_Web_Services.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#web-services"></cta-button>

## Search Engines

- [Search Engines: Information Retrieval in Practice - W. Bruce Croft, Donald Metzler, Trevor Strohman (PDF)](https://ciir.cs.umass.edu/irbook/)

- [Solr for newbies workshop (2019) - Hector Correa ](https://github.com/hectorcorrea/solr-for-newbies)[ -- (PDF)](https://github.com/hectorcorrea/solr-for-newbies/blob/main/tutorial.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#search-engines"></cta-button>