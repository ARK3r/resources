---
title: Courses
description: We're compiling useful web development courses around the internet to help you understand web .
position: 2503
category: The Web
---

## Fullstack Web Development 

Learn React, Redux, Node.js, MongoDB, GraphQL and TypeScript in one go! This course will introduce you to modern JavaScript-based web development. The main focus is on building single page applications with ReactJS that use REST APIs built with Node.js.

<cta-button text="Full Course" link="https://fullstackopen.com/en/"></cta-button>

## Web Development

The open curriculum for learning web development

<cta-button text="Learn More" link="https://github.com/TheOdinProject/curriculum"></cta-button>
