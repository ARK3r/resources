---
title: Courses
description: We're compiling useful design courses around the internet to help you learn UX design.
position: 1107
category: Design 
---

## Beginners Guide to Graphic Design

If you're interested in Graphic Design and considering becoming a Graphic Designer then join Gareth David as he discuss a series of graphic design topics.

From what graphic design is, Skills to be a graphic designer, Design theory, Education you need, equipment you need, to the graphic design portfolio and interview advice, this series is for anyone at any level.

<youtube-video id="WONZVnlam6U?list=PLYfCBK8IplO4E2sXtdKMVpKJZRBEoMvpn"> </youtube-video>


## Curriculum for Self Taught Designers

This podcast is all about what do you need to know to be a successful graphic designer or creative person in the 21st century knowledge economy? What do you need to learn if you're self taught or have a general design education?

<youtube-video id="yRzfNvdgjhA"> </youtube-video>

## Design Critiques

This playlist has a series of design critique videos that will help you improve your designing skills. 

<youtube-video id="playlist?list=PLroLjS4HDi0BDo2r9yVstsvFbGBi8QUwZ"> </youtube-video>


## Degreeless Design

This website almost includes everything that you need to learn about design. It covers all the design aspects from the begineer to expert level.

<cta-button text="Learn More" link="https://www.degreeless.design/"></cta-button>

## The Ultimate Guide to Becoming a Great Designer

This three step design tutorial by Jon Moore has some very interesting tips that will simplify your thought process and make your life easier as a designer.

<cta-button text="Part 1" link="https://modus.medium.com/the-ultimate-guide-to-becoming-a-great-designer-part-1-of-3-88f627233f48"></cta-button>

<cta-button text="Part 2" link="https://modus.medium.com/the-ultimate-guide-to-becoming-a-great-designer-part-2-of-3-9d79f8f30703"></cta-button>

<cta-button text="Part 3" link="https://modus.medium.com/the-ultimate-guide-to-becoming-a-great-designer-part-3-of-3-591816a4a4b1"></cta-button>

## Design Masterclass

This video will help you to take a design from an idea on paper to reality. What does it look like to create something from just an idea, and how do you do it? Raphael walks us through his process, which is a master class on how you can do this for yourself.

<youtube-video id="2MrNSjJFBBI"> </youtube-video>



