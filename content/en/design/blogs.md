---
title: Blogs
description: We're compiling useful resources around the internet to help you learn UX design.
position: 1109
category: Design
---

## How to start in UX

A free, self-guided class to help you take your first steps into digital product design.

<cta-button text="Learn More" link="https://start.uxdesign.cc/"></cta-button>

## "You don’t need to know everything about UX" *by Fabricio Teixeira*

This blog by Fabricio Teixeira counsel the UX designers that why they don’t need to be a specialist in all possible verticals within User Experience Design.

<cta-button text="Learn More" link="https://uxdesign.cc/journey/home"></cta-button>

## What They Don’t Tell You About The UX Industry Starting Out

This article has a lot of potential advices regarding User Experience industry and  will help you get a firm grasp of what UX is.

<cta-button text="Learn More" link="https://bootcamp.uxdesign.cc/what-they-dont-tell-you-about-the-ux-industry-starting-out-7f2dfc1617cf"></cta-button>

## UX vs UI vs IA vs IxD : 4 Confusing Digital Design Terms Defined

This blog by Vincent Xia explains the difference between four digital design terms.

<cta-button text="Learn More" link="https://uxplanet.org/ux-vs-ui-vs-ia-vs-ixd-4-confusing-digital-design-terms-defined-1ae2f82418c7"></cta-button>

## User Experience for Product Designers

This blog by Paul Hershey explains five simple steps product designers can use to build better web apps.

<cta-button text="Learn More" link="https://medium.com/looks-good-feels-good/user-experience-for-product-designers-e9fa621ce3bc"></cta-button>

## How to get a Job as UX Designer

This blog by Amy Smith explains 5 simple steps that you can follow to land a job as UX Designer with no experience.

<cta-button text="Learn More" link="https://soulless.medium.com/5-simple-steps-to-land-a-job-as-ux-designer-with-no-experience-ac7729ca0c7a"></cta-button>

## How to Become a UX / UI Designer, with no Work Experience and Degree

This blog by Sati Taschiba gives a detailed overview of 9 steps that you can follow to become a UX / UI designer, if you do not have a work experience and a degree.

<cta-button text="Learn More" link="https://blog.prototypr.io/9-steps-how-to-become-a-ux-ui-designer-if-you-do-not-have-a-work-experience-and-a-degree-15c488824c81"></cta-button>

## How to build a UX portfolio

This blog by Fabricio Teixeira explains how to build a UX portfolio if you have never worked in UX?

<cta-button text="Learn More" link="https://uxdesign.cc/how-to-build-a-ux-portfolio-if-i-have-never-worked-in-ux-80ebab8f3407"></cta-button>

## 4 steps for choosing the right projects for your UX portfolio

This article will give you a framework for deciding which projects make sense for you and your career goals.

<cta-button text="Learn More" link="https://www.invisionapp.com/inside-design/4-steps-for-choosing-projects/"></cta-button>

## How to Craft an Outstanding Case Study for Your UX Portfolio

UX case study relies on excellent storytelling with a clear, understandable structure.

This article breaks down the anatomy of a UX case study to help you tell a simple and effective story that shows off your skills.

<cta-button text="Learn More" link="https://careerfoundry.com/en/blog/ux-design/ux-case-study/"></cta-button>

## The Case Study Factory

Case Study Factory is a platform which facilitates many students and designers who are looking for an opportunity to have their work reach a broader audience. 

<cta-button text="Learn More" link="https://essays.uxdesign.cc/case-study-factory/"></cta-button>







