---
title: Typography
description: We're compiling useful resources around the internet to help you learn typography.
position: 1105
category: Design
---

## Typography 

Typography design is the art of arranging a message in a readable and aesthetically pleasing composition. 

<cta-button text="Learn More" link="https://99designs.com/blog/tips/typography-design/"></cta-button>

## Typography Critique

Learn typography with Chris Do as he reviews user-submitted work from his class and goes over the fundamentals of good typography. 

<youtube-video id="4h4RNmemzB0?list=PLroLjS4HDi0BmcTki-J7ax5M0GapbLQrW"> </youtube-video>

## How To Improve Your Layout and Typography Critique

This video will help you improve your graphic design skills and learn how to use type. Typography critique and review by Chris Do and the Futur team.

<youtube-video id="xjAQ5CK4oqg"> </youtube-video>

## Learn Typography Through This Poster Design Critique

Don't get stuck with your type layouts. Improve your typography skills by watching Chris Do & Emily Xie review and critique fan submitted design and layouts. They offer up helpful suggestions, design alternatives and explore different design options.

<youtube-video id="VH8QT73Gxwo"> </youtube-video>

<youtube-video id="0fvEFIkT7pM"> </youtube-video>

