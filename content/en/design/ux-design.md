---
title: UX Design
description: We're compiling useful UX design resources around the internet to help you learn digital design.
position: 1103
category: Design
---

## User Experience Design

In this guide, you will learn about the basics of User experience design. According to Wikipedia, it is defined as the process of creating evidence-based interaction designs between human users and products or websites. 

[![User experience design](https://i.imgur.com/Is0dy9T.png)](https://en.wikipedia.org/wiki/User_experience_design)

[Source: wikipedia.org](https://en.wikipedia.org/wiki/Main_Page)

<cta-button text="View" link="https://en.wikipedia.org/wiki/User_experience_design"></cta-button>


## What Is User Experience (UX) Design? Everything You Need to Know

In this guide, you will learn about UX design, the immense value of UX design to the world, and everything about getting started in this exciting industry.

[![What Is User Experience (UX) Design? Everything You Need to Know](https://i.imgur.com/4pa9OpK.png)](https://careerfoundry.com/en/blog/ux-design/what-is-user-experience-ux-design-everything-you-need-to-know-to-get-started/)

[Source: careerfoundry.com](https://careerfoundry.com/)

<cta-button text="View" link="https://careerfoundry.com/en/blog/ux-design/what-is-user-experience-ux-design-everything-you-need-to-know-to-get-started/"></cta-button>


## What You Should Know About User Experience Design

In this article, you will learn:

- What is UX?
- What is UX design?
- Why should you care about UX?
- What does a UX designer do?

[![What You Should Know About User Experience Design](https://i.imgur.com/JRlvmvg.png)](https://xd.adobe.com/ideas/career-tips/what-is-ux-design/)

[Source: xd.adobe.com](https://xd.adobe.com/ideas/)

<cta-button text="View" link="https://xd.adobe.com/ideas/career-tips/what-is-ux-design/"></cta-button>


## What is User Experience (UX) Design?

User experience (UX) design is the process design teams use to create products that provide meaningful and relevant experiences to users. This involves the design of the entire process of acquiring and integrating the product, including aspects of branding, design, usability and function.

[![What is User Experience (UX) Design](https://i.imgur.com/SNawqlG.png)](https://www.interaction-design.org/literature/topics/ux-design)

[Source: interaction-design.org](https://www.interaction-design.org/)

<cta-button text="View" link="https://www.interaction-design.org/literature/topics/ux-design"></cta-button>


## What Is UX Design? A Guide for Beginners in 2022

User experience design, or UX design, encompasses the ways in which people engage with products. UX design is the process of creating a positive user experience that meets a user’s needs. Effective UX design enables users to achieve their goals easily, efficiently, and with minimal friction.

[![What Is UX Design? A Guide for Beginners in 2022](https://i.imgur.com/6xJkXmP.png)](https://www.springboard.com/blog/design/what-is-ux-design-user-experience-experts-weigh-in/)

[Source: springboard.com](https://www.springboard.com/)

<cta-button text="View" link="https://www.springboard.com/blog/design/what-is-ux-design-user-experience-experts-weigh-in/"></cta-button>


##  Design + UX. What Is UX Design?

In this guide, you will learn about:

- Design overview.

- UX Design salary and job outlook.

- How to become a UX designer.


[![Design + UX. What Is UX Design?](https://i.imgur.com/ycotuL9.png)](https://builtin.com/design-ux)

[Source: builtin.com](https://builtin.com/)

<cta-button text="View" link="https://builtin.com/design-ux"></cta-button>

## What is UX Design?

User Experience Design (UXD) is the process of enhancing user satisfaction by improving the usability, accessibility, and pleasure provided in the interaction between the user and a product or service.

In this video Jose and Chris explain what UX Design is by working on the redesign of The Skool's website.

<youtube-video id="CJnfAXlBRTE?list=PLzKJi2GjpkEELRD-YatYYLyItQ9O_MPU8"> </youtube-video>

## 5 Principles of Visual Design in UX

Visual-design principles inform us how design elements such as line, shape, color, grid, or space go together to create well-rounded and thoughtful visuals.

This article defines 5 visual-design principles that impact UX:

- Scale
- Visual hierarchy
- Balance
- Contrast
- Gestalt

[![5 Principles of Visual Design in UX](https://i.imgur.com/tFWH1bL.png)](https://www.nngroup.com/articles/principles-visual-design/)

[Source: nngroup.com](https://www.nngroup.com/)

<cta-button text="Learn More" link="https://www.nngroup.com/articles/principles-visual-design/"></cta-button>


## Laws of UX

Learn about laws of UX, which is a collection of best practices that designers can consider when building user interfaces.

[![Laws of UX](https://i.imgur.com/5ObLvix.png)](https://lawsofux.com/)

[Source: lawsofux.com](https://lawsofux.com/en/)

<cta-button text="Learn More" link="https://lawsofux.com/"></cta-button>


## UX movement

It is a user experience design publication dedicated to teaching people how to make interfaces more intuitive and easier to use.

[![UX movement](https://i.imgur.com/4zIIT1u.png)](https://uxmovement.com/)

[Source: uxmovement.com](https://uxmovement.com/)

<cta-button text="Learn More" link="https://uxmovement.com/"></cta-button>


## User Research (UXR)

A collection of 250+ resources & tools dedicated to user research, that will help you incorporate it into your design practice.

[![User Research (UXR](https://i.imgur.com/veAVCw3.png)](http://guidetouxr.com/)

[Source: guidetouxr.com](http://guidetouxr.com/)

<cta-button text="Learn More" link="http://guidetouxr.com/"></cta-button>

