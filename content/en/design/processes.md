---
title: Processes
description: We're compiling useful design processes around the internet to help you on your design journey.
position: 1118
category: Design 
---

## UX Design Process

UX Design process involves the design of the entire process of acquiring and integrating the product, including aspects of branding, design, usability and function.

This video will help you learn how to use "task gap opportunity" in your UX design process to identify unmet needs

<youtube-video id="iE39TOuVvWw?list=PLzKJi2GjpkEELRD-YatYYLyItQ9O_MPU8"> </youtube-video>

## Design Sprints

These videos will help you understand the Google design sprint method developed by Jake Knapp to solve big problems and test new ideas in just four days.

Moreover it will help you perceive how your idea look like in real life, and how many meetings and discussions it will take before you can be sure you have the right solution?

**Part 1**

<youtube-video id="gAVIkM2eJBw?list=PLzKJi2GjpkEELRD-YatYYLyItQ9O_MPU8"> </youtube-video>

**Part 2**

<youtube-video id="5FxvnGzTBqw?list=PLzKJi2GjpkEELRD-YatYYLyItQ9O_MPU8"> </youtube-video>
