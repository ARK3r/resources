---
title: Logo
description: We're compiling useful resources around the internet to help you learn logo design.
position: 1106
category: Design 
---

## What Makes A Logo Great & Iconic?

What is a good logo? What is a bad logo? A logo is not communication. It's identification. It's the period at the end of sentence and not the sentence. How do you create a simple but not generic logo? Should a logo be complicated? How can you justify the price of a simple logo? Is a simple logo easy or hard to create? How can you make a timeless logo?

Logos are never love at first sight. It takes time to develop. A trademark gains meaning and power over time. Who 

Chermayeff & Geismar & Haviv is the brand design firm behind many of the world’s most recognizable trademarks. Since 1957, the firm has pioneered the modern movement of idea-driven graphic design across every discipline, specializing in brand identities, exhibitions, print and motion graphics, and art in architecture.

<youtube-video id="Fz-XGd8EX3U"> </youtube-video>

## Logo Design Principles & Methodology

This video will guide you to how to make a logo better? How do you improve your logo design?  Does your logo look bad and you don't know why? What are basic logo design fundamentals?

<youtube-video id="6ZAD6jxgGwk"> </youtube-video>


## Logo Design Therapy Ep 1

This is Logo Therapy with Jelvin Base and Mihai Dolganiuc will help you learn how professionals deal with logo challenges. One is a hand lettering expert while the other creates amazing monograms and geometric logos.

<youtube-video id="4ZeEfF2O8-Q"> </youtube-video>

## Logo Therapy Ep. 2 Design Critique

2 logos, 2 designers, 2 countries— 30 minutes of logo design review and critique. See how professional designers tackle  logo challenges.

<youtube-video id="-JYiXyzDEFI"> </youtube-video>

## Logo Therapy Ep. 03 Logo Critique & Review

Ongoing series where 2 designers critique 2 logos that are submitted by their audience. 2 logos, 2 designers, 2 countries— 30 minutes of logo design review and critique. See how professional designers tackle logo challenge.

<youtube-video id="V6e-rFgkZzI"> </youtube-video>


## Logo Design Process— Live feat. Spartan Logo Design Challenge

In this video logo design process is explained along with updates and critique on the Spartan Logo Design Challenge.

<youtube-video id="w6nvr_V0E68"> </youtube-video>


## Spartan Logo Design Challenge pt. 2

Midpoint logo critique for a handful of participants in the Spartan Logo Design Challenge. Chris and Ben critique logos live on this live stream.

<youtube-video id="aPguPSY-e_4"> </youtube-video>

## RAW: Spartan Logo Design Winner Announcement & Design Critique

Over 1200 entries were reviewed and the winner of the Spartan Logo Design Challenge was selected. Chris and Ben share their favorite logos, offer constructive criticism and how they judged the entries.

<youtube-video id="yKHc2or01Iw"> </youtube-video>

| Website&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; | Description |
| ----------------------- | ------------------ |
| [Instant Logo Search](http://instantlogosearch.com/)| Thousands of free brands logos ( SVG - PNG ) |
| [LogoSear.ch](https://logosear.ch/search.html) | Search engine with over 200,000 SVG logos indexed |
| [SVGPorn](https://svgporn.com) | 1000+ high-quality SVG logos |
| [Payment System Logos](https://github.com/mpay24/payment-logos/) | Logos for payment systems available in png and svg |
| [Browser Logos](https://github.com/alrra/browser-logos/) | High resolution web browser logos |
| [VectorLogoZone](https://www.vectorlogo.zone/) | Consistently formatted SVG logos |
| [World Vector Logo](https://worldvectorlogo.com/)| Download vector logos of brands you love |
| [Logo Maker](https://logomakr.com/)| Create custom logos |
| [Free Logo Maker](https://www.namecheap.com/logo-maker/)| Fast, All-in-One Logo Generator |
| [LOGOwine](https://www.logo.wine/)| Brand Logos Free Download in SVG Vector & PNG File Format |
| [Namecheap Logo Maker](https://www.namecheap.com/logo-maker/)| Create and download custom Logos for free |



