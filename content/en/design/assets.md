---
title: Assets
description: We're compiling useful design assets around the internet to help you with your design skills.
position: 1108
category: Design
---

## Eva Design Assets

Eva Design is customizable design system with mobile and web component libraries.

Its design assets easily adaptable to your brand. Available for Sketch and Figma component libraries, based on Eva Design System.

<cta-button text="Learn More" link="https://eva.design/"></cta-button>


## Sustain Open Source Design

S.O.S. Design is a podcast dedicated to exploring the intersection of open source and design: 

- How design is crucial in the open source ecosystem?
- How designers work with coders to make open source software better?
- What sustainability means for the field of open source designers? 

This podcast grew out of the Sustain community and Open Source Design, and seeks to share great conversations with members from both communities and the open source and design space at large.

<cta-button text="Learn More" link="https://sosdesign.sustainoss.org/"></cta-button>

##  Open Source Design

This amazing resource is a showcase of the many websites and platforms where you can find openly licensed icons, fonts, image, tools and other resources.

You can use them for any purpose, also commercial (some works have specific licenses, so always make sure it’s fine to use).

<cta-button text="Learn More" link="https://opensourcedesign.net/resources/"></cta-button>

## Design Articles 

A platform where you can find some insightful articles about design systems.

<cta-button text="Learn More" link="https://designsystemsrepo.com/articles/"></cta-button>

## Design Systems Gallery

A comprehensive and curated list of design systems, style guides and pattern libraries that you can use for inspiration.

<cta-button text="Learn More" link="https://designsystemsrepo.com/design-systems/"></cta-button>

## Penpot

Penpot is the first Open Source design and prototyping platform meant for cross-domain teams. 

Non dependent on operating systems, Penpot is web based and works with open web standards (SVG). For all and empowered by the community.

<cta-button text="Learn More" link="https://penpot.app/"></cta-button>

## Learn how to become a better designer.

You can be a student in university, intern at a company or senior designer at a startup, these resources will help you to level up your work.

<cta-button text="Learn More" link="https://1984.design/"></cta-button>

## UX Designer Tools and Resources

This is the best collection of UX Designer tools and resources gathered from various conferences and workshops in Istanbul.

<cta-button text="Learn More" link="https://medium.com/@UXAliveTurkey/the-best-ux-designer-tools-resources-collection-24bf115d17bc"></cta-button>

## A Visual Guide To Best UX Books To Read

UX Design is about addressing issues and making people’s life simpler by using intuitive and effective design. This curated list of UX books will teach you the fundamentals in a practical and precise way.

It’s a combination of design, product, and business reads. Knowledge of HCI, usability, design principles, as well as knowing parts of psychology and how to design for people, is essential for any UX Position.

<cta-button text="Learn More" link="https://medium.com/@linhduy/a-visual-guide-to-best-ux-books-to-read-6700342c3aef"></cta-button>

## UX Design Books: What UXers Must Read in 2020 ？

Good design is never easy. It doesn’t matter where you’re at in UX design - beginner, intermediate, or expert, you should keep learning and working hard to improve your design skills.

UX books, one of the most essential learning resources, are the best way to learn UX design basics and improve your design skills quickly and systematically.

<cta-button text="Learn More" link="https://www.mockplus.com/blog/post/ux-design-books"></cta-button>

## Design Resources 

A vast range of design resources on Github. 

<cta-button text="Learn More" link="https://github.com/bradtraversy/design-resources-for-developers"></cta-button>

A curated list of the best design resources handpicked from around the web.

<cta-button text="Learn More" link="https://designresourc.es/s"></cta-button>

List of free Product (& UI/UX) Design resources.

<cta-button text="Learn More" link="https://www.notion.so/552ed20b2c2948cc8b284e59230d8868?v=3705e6f9547b40bc9301d8f83403577e"></cta-button>

## Remote Design Resources from the Community

This is one of the great platforms for Figma Community coming together to share resources, learnings, and best practices as designers learn new ways of working together remotely.

<cta-button text="Learn More" link="https://www.notion.so/figmacommunity/Remote-Design-Resources-from-the-Community-e2af55fa7ace484bbe66d98ba3fd2020"></cta-button>


## Design Newsletter

HeyDesigner is the daily curated design knowledge for product people, UXers, PMs, and design engineers.

<cta-button text="Learn More" link="https://heydesigner.com/topics/"></cta-button>

## Designmodo: Create Website and Email Newsletter Design

All-in-one solution to create website and newsletter design to impress and engage your customers.

- **Drag & Drop Builder:** Choose a pre-designed template and make it your own.

- **Custom Fonts:** Pick your free font to make your text stand out.

- **HTML/CSS & Javascript:** Get all the flexibility you need with custom setups for advanced users.

- **Upload Images and Video:** Easily add imagery and video to make sure your message hits home.

- **Updated Design:** Stay on trend with the latest design styles, with regular free updates.

- **Animation Effects:** Make your page more live!

<cta-button text="Learn More" link="https://designmodo.com/"></cta-button>


## UX Library

Your one-stop website for UX Articles, Books, Resources, & More

<cta-button text="Learn More" link="https://www.uxlibrary.org/"></cta-button>


