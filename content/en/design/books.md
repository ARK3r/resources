---
title: Books
description: We're compiling a library of useful Graphics' books from around the Internet to help you on your software journey!
position: 1110
category: Design
---


## Graphical User Interface

### Web Style Guide Online 

<book-cover link="https://www.webstyleguide.com/wsg3/index.html" img-src="https://i.imgur.com/ATENdvJ.jpg" alt="Book cover for Web Style Guide Online "> </book-cover>


This book is an ideal reference for web site designers in corporations, government, nonprofit organizations, and academic institutions, the book explains established design principles and covers all aspects of web design—from planning to production to maintenance. The guide also shows how these principles apply in web design projects whose primary concerns are information design, interface design, and efficient search and navigation.

<cta-button text="Complete Book" link="https://www.webstyleguide.com/wsg3/index.html"></cta-button>


### Search User Interfaces 

<book-cover link="http://searchuserinterfaces.com/book/" img-src="https://i.imgur.com/5jmgp4F.jpg" alt="Book cover for Search User Interfaces "> </book-cover>

This book focuses on the human users of search engines and the tool they use to interact with them: the search user interface. The truly worldwide reach of the Web has brought with it a new realization among computer scientists and laypeople of the enormous importance of usability and user interface design. Researchers and practitioners have developed a wide range of innovative interface ideas, but only the most broadly acceptable make their way into major web search engines.

This book summarizes these developments, presenting the state of the art of search interface design, both in academic research and in deployment in commercial systems. Many books describe the algorithms behind search engines and information retrieval systems, but the unique focus of this book is specifically on the user interface. It will be welcomed by industry professiona

<cta-button text="Complete Book" link="http://searchuserinterfaces.com/book/"></cta-button>

### Web Design Primer 

<book-cover link="https://pressbooks.library.ryerson.ca/webdesign/" img-src="https://i.imgur.com/ie0jUza.png" alt="Book cover for Web Design Primer "> </book-cover>

The goal of the book is to provide students with a reference on some of the latest web design practices that is short and to-the-point, low-cost, and readily accessible.

<cta-button text="Complete Book" link="https://pressbooks.library.ryerson.ca/webdesign/"></cta-button>

</br>

<cta-button text="More Books On Design" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#graphical-user-interfaces"></cta-button>
