---
title: Figma
description: We're compiling useful resources around the internet to help you learn figma.
position: 1104
category: Design
---

## Figma Resources

Figma is a collaborative interface design tool that's taking the design world by storm. Unlike Sketch, which runs as a standalone MacOS app, Figma is entirely browser-based, and therefore works not only on Macs, but also on PCs running Windows or Linux, and even on Chromebooks. It also offers a web API, and it's free!

<cta-button text="Learn More" link="https://liferay.design/resources/figma/"></cta-button>

## Our Figma File

<cta-button text="View" link="http://figma.grey.software"></cta-button>

