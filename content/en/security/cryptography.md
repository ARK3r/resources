---
title: Cryptography 
description: We're compiling useful resources around the internet to help you understand Cryptography!
position: 8000
category: Cyber Security 
---

## Cryptography Concepts for Node.js Developers

The mysterious discipline of cryptography is the backbone of the internet. Without it, there would be no secrets and no privacy in the digital world. As a developer, you don’t need to understand the math that goes into cryptography, but it’s absolutely essential to know key concepts like hashes, salt, keypairs, encryption, and signing.

The video and accompanying tutorial explain essential cryptography concepts and implements then with the builtin Node.js crypto module.

<cta-button  link="https://fireship.io/lessons/node-crypto-examples/" text="Tutorial" > </cta-button>

<youtube-video id="NuyzuNBFWxQ"></youtube-video>
