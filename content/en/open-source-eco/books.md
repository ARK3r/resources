---
title: Books
description: We're compiling a library of useful Open Source Ecosystem books from around the Internet to help you on your software journey!
position: 17500
category: Open Source Ecosystem
---

## Open Source Ecosystem

### Free as in Freedom: Richard Stallman and the free software revolution 

<book-cover link="https://archive.org/details/faif-2.0" img-src="https://i.imgur.com/8i8YTR1.jpg" alt="Book cover for Free as in Freedom: Richard Stallman and the free software revolution"> </book-cover>

In 2002, Sam Williams wrote Free as in Freedom, a biography of Richard M. Stallman. In its epilogue, Williams expressed hope that choosing to distribute his book under the GNU Free Documentation License would enable and encourage others to share corrections and their own perspectives through modifications to his work. 

Free as in Freedom (2.0) is Stallman's revision of the original biography. While preserving Williams's viewpoint, it includes factual corrections and extensive new commentary by Stallman, as well as new prefaces by both authors written for the occasion. It is a rare kind of biography, where the reader has the benefit of both the biographer's original words and the subject's response.

<cta-button text="Complete Book" link="https://archive.org/details/faif-2.0"></cta-button>


### The Architecture of Open Source Applications

<book-cover link="http://www.aosabook.org/en/index.html" img-src="https://i.imgur.com/PeqUuns.jpg" alt="Book cover for The Architecture of Open Source Applications"> </book-cover>

Architects look at thousands of buildings during their training, and study critiques of those buildings written by masters. In contrast, most software developers only ever get to know a handful of large programs well - usually programs they wrote themselves - and never study the great programs of history. As a result, they repeat one another's mistakes rather than building on one another's successes. This book's goal is to change that. In it, the authors of twenty-five open source applications explain how their software is structured, and why. What are each program's major components? How do they interact? And what did their builders learn during their development? In answering these questions, the contributors to this book provide unique insights into how they think.

<cta-button text="Complete Book" link="http://www.aosabook.org/en/index.html"></cta-button>


### Open Advice: FOSS: What We Wish We Had Known When We Started

<book-cover link="http://open-advice.org/" img-src="https://i.imgur.com/mprjB6v.jpg" alt="Book cover for Open Advice: FOSS: What We Wish We Had Known When We Started"> </book-cover>

Open Advice is a knowledge collection from a wide variety of Free Software projects. It answers the question what 42 prominent contributors would have liked to know when they started so you can get a head-start no matter how and where you contribute

<cta-button text="Complete Book" link="http://open-advice.org/"></cta-button>

</br>

<cta-button text="More Books On Open Source Ecosystem" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#open-source-ecosystem"></cta-button>