---
title: Cheat Sheets
description: We're compiling a library of useful cheet sheats from around the Internet to help you build technologies in machine learning!
position: 9003
category: Machine Learning
---



## Tensorflow 

### TensorFlow Quick Reference Table

TensorFlow is very popular deep learning library, with its complexity can be overwhelming especially for new users. Here is a short summary of often used functions.

[![TensorFlow Quick Reference Table Preview](https://i.imgur.com/ZLW4Tzm.png)](https://secretdatascientist.com/tensor-flow-cheat-sheet/)

<cta-button text="View" link="https://secretdatascientist.com/tensor-flow-cheat-sheet/"></cta-button>


### TensorFlow v2.0 Cheat Sheet

TensorFlow is an open-source software library for highperformance numerical computation. Its flexible architecture
enables to easily deploy computation across a variety of platforms (CPUs, GPUs, and TPUs), as well as mobile and edge
devices, desktops, and clusters of servers. TensorFlow comes with strong support for machine learning and deep learning

[![TensorFlow v2.0 Cheat Sheet Preview](https://i.imgur.com/PueJwJj.png)](https://web.archive.org/web/20200922212358/https://www.aicheatsheets.com/static/pdfs/tensorflow_v_2.0.pdf)

<cta-button text="View" link="https://web.archive.org/web/20200922212358/https://www.aicheatsheets.com/static/pdfs/tensorflow_v_2.0.pdf"></cta-button>

</br>

<cta-button text="More Cheatsheets On Tensorflow" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#tensorflow"></cta-button>


## PyTorch

PyTorch is an open source machine learning framework based on the Torch library, used for applications such as computer vision and natural language processing, primarily developed by Meta AI. It is free and open-source software released under the Modified BSD license.

### PyTorch Framework Cheat Sheet 

[![PyTorch Framework Cheat Sheet Preview](https://i.imgur.com/8D95yLB.png)](https://www.simonwenkel.com/publications/cheatsheets/pdf/cheatsheet_pytorch.pdf)

<cta-button text="View" link="https://www.simonwenkel.com/publications/cheatsheets/pdf/cheatsheet_pytorch.pdf"></cta-button>

### PyTorch Official Cheat Sheet 

[![PyTorch Official Cheat Sheet Preview](https://i.imgur.com/PtJI3he.png)](https://pytorch.org/tutorials/beginner/ptcheat.html)

<cta-button text="View" link="https://pytorch.org/tutorials/beginner/ptcheat.html"></cta-button>

</br>

<cta-button text="More Cheatsheets On PyTorch" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#pytorch"></cta-button>


## Python

Python is a general-purpose, versatile, and powerful programming language. It’s a great first language because it’s concise and easy to read. Whatever you want to do, Python can do it. From web development to machine learning to data science, Python is the language for you

### Comprehensive Python Cheatsheet 

[![Comprehensive Python Cheatsheet Preview](https://i.imgur.com/8UJGlg6.png)](https://gto76.github.io/python-cheatsheet/)

<cta-button text="View" link="https://gto76.github.io/python-cheatsheet/"></cta-button>

### Learn Python in Y minutes 

[![Learn Python in Y minutes Preview](https://i.imgur.com/bITb8TT.png)](https://learnxinyminutes.com/docs/python/)

<cta-button text="View" link="https://learnxinyminutes.com/docs/python/"></cta-button>

### Official Matplotlib cheat sheets 

[![Official Matplotlib cheat sheets](https://i.imgur.com/ACZL5qx.png)](https://github.com/matplotlib/cheatsheets)

<cta-button text="View" link="https://github.com/matplotlib/cheatsheets"></cta-button>

</br>

<cta-button text="More Cheatsheets On Python" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#python"></cta-button>


## R

R is a programming language for statistical computing and graphics supported by the R Core Team and the R Foundation for Statistical Computing.

### All RStudio cheatsheets resources 

[![All RStudio cheatsheets resources Preview](https://i.imgur.com/D97iUyJ.png)](https://www.rstudio.com/resources/cheatsheets/)

<cta-button text="View" link="https://www.rstudio.com/resources/cheatsheets/"></cta-button>

</br>

<cta-button text="More Cheatsheets On R" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#r"></cta-button>