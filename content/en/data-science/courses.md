---
title: Courses
description: We're compiling useful data science courses from top universities to help you on your software journey!
position: 10500
category: Data Science
---

## Introduction to Computational Thinking and Data Science (MIT)

This course aims to provide students with an understanding of the role computation can play in solving problems and to help students, regardless of their major, feel justifiably confident of their ability to write small programs that allow them to accomplish useful goals. The class uses the Python 3.5 programming language.

<youtube-video id="C1lhuz6pZC0?list=PLUl4u3cNGP619EG1wp0kT-7rDE_Az5TNd"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/watch?list=PLUl4u3cNGP619EG1wp0kT-7rDE_Az5TNd&v=C1lhuz6pZC0"></cta-button>

## Data Analysis with Dr. Mike Pound

In this course, Dr Mike Pound will help you understand how to refine your data, clean up datasets, visualise the information & extract meaningful knowledge from your dataset.

<youtube-video id="NxYEzbbpk-4?list=PLzH6n4zXuckpfMu_4Ff8E7Z1behQks5ba"> </youtube-video>

<cta-button  link="https://www.youtube.com/playlist?list=PLzH6n4zXuckpfMu_4Ff8E7Z1behQks5ba" text="View Playlist!" > </cta-button>
