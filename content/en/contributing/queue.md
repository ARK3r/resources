---
title: Adding to Queue
description: Learn about how to add resources to our queue.
category: Contributing
position: 101
---

## When should you add to the queue

If you're unsure about where to share a resource or you don't have enough time, then simply paste the resource's link in [queue](/queue) page.
