---
title: Methods
description: We're curating useful resources about javascript methods from around the Internet to help you on your software journey!
position: 3004
category: Javascript
---

## Array Map in Javascript

#### Array.prototype.map()

Sometimes you may need to take an array and apply a function to its elements so that you get a new array with modified elements.

Instead of manually iterating over the array using a loop, you can simply use the built-in Array.map() method.

The Array.map() method allows you to iterate over an array and modify its elements using a callback function. The callback function is executed on each of the array's elements and returns a transformed (or mapped) version of the element.

<youtube-video id="DC471a9qrU4?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>

## Array Reduce in Javascript

#### Array.prototype.reduce()

This JavaScript method is used to reduce the array to a single value.
reduce() and executes a provided function for each value of the array (from left-to-right), and the return value of the function is stored in an accumulator parameter.

<youtube-video id="tVCYa_bnITg?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>
