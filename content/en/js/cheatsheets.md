---
title: Cheat Sheets
description: We're curating useful JavaScript cheatsheets from around the Internet to help you on your software journey!
position: 3008
category: Javascript
---

## Javascript 

### Modern JavaScript Cheatsheet

[![Modern JavaScript Cheatsheet Preview](https://i.imgur.com/NMlqyUY.png)](https://github.com/mbeaudru/modern-js-cheatsheet)

[Source : mbeaudru from Github](https://github.com/mbeaudru)

<cta-button text="View" link="https://github.com/mbeaudru/modern-js-cheatsheet"></cta-button>


### Learn JavaScript in Y minutes

[![Learn JavaScript in Y minutes Preview](https://i.imgur.com/pP4mApd.png)](https://learnxinyminutes.com/docs/javascript/)

[Source : learnxinyminutes.com](https://learnxinyminutes.com/)

<cta-button text="View" link="https://learnxinyminutes.com/docs/javascript/"></cta-button>


### JavaScript Cheatsheet 

[![JavaScript CheatSheet Preview](https://i.imgur.com/wBF5anb.png)](https://www.codecademy.com/learn/introduction-to-javascript/modules/learn-javascript-introduction/cheatsheet)

[Source : Codecademy](https://www.codecademy.com/)

<cta-button text="View" link="https://www.codecademy.com/learn/introduction-to-javascript/modules/learn-javascript-introduction/cheatsheet"></cta-button>


### JS CheatSheet

[![JS CheatSheet  Preview](https://i.imgur.com/OLT6sQO.png)](https://htmlcheatsheet.com/js/)

[Source : HTMLCheatSheet](https://htmlcheatsheet.com/)

<cta-button text="View" link="https://htmlcheatsheet.com/js/"></cta-button>


### JavaScript Cheat Sheet by DaveChild

[![JavaScript Cheat Sheet by DaveChild Preview](https://i.imgur.com/HYyYP8W.png)](https://cheatography.com/davechild/cheat-sheets/javascript/)

[Source : Dave Child](https://aloneonahill.com/)

<cta-button text="View" link="https://cheatography.com/davechild/cheat-sheets/javascript/"></cta-button>


### JavaScript Regex Cheatsheet

[![JavaScript Regex Cheatsheet Preview](https://i.imgur.com/3QPyuZ5.png)](https://www.debuggex.com/cheatsheet/regex/javascript)

[Source : Debuggex](https://www.debuggex.com/)

<cta-button text="View" link="https://www.debuggex.com/cheatsheet/regex/javascript"></cta-button>


</br>

<cta-button text="More Cheatsheets On Javascript" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#javascript"></cta-button>


## jQuery 

### jQuery CheatSheet

[![jQuery CheatSheet Preview](https://i.imgur.com/QWsO7vL.png)](https://htmlcheatsheet.com/jquery/)

[Source : HTMLCheatSheet](https://htmlcheatsheet.com/)

<cta-button text="View" link="https://htmlcheatsheet.com/jquery/"></cta-button>

</br>

<cta-button text="More Cheatsheets On jQuery" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#jquery"></cta-button>


## Nest.js

### Nest.js CheatSheet 

[![Nest.js CheatSheet Preview](https://i.imgur.com/BWQHM1X.png)](https://gist.github.com/guiliredu/0aa9e4d338bbeeac369a597e87c9ba46)

[Source: guiliredu from Github](https://gist.github.com/guiliredu)

<cta-button text="View" link="https://gist.github.com/guiliredu/0aa9e4d338bbeeac369a597e87c9ba46"></cta-button>

</br>

<cta-button text="More cheatsheets On Nest.js" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#nestjs"></cta-button>


## Nuxt.js

### Nuxt.js Essentials Cheatsheet 

[![Nuxt.js Essentials Cheatsheet Preview](https://i.imgur.com/CYIDXbB.png)](https://www.vuemastery.com/pdf/Nuxtjs-Cheat-Sheet.pdf)

[Source: VueMastery](https://www.vuemastery.com/)

<cta-button text="View" link="https://www.vuemastery.com/pdf/Nuxtjs-Cheat-Sheet.pdf"></cta-button>

</br>

<cta-button text="More Cheatsheets On Nuxt.js" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#nuxtjs"></cta-button>


## React.js

### React Cheatsheet 

[![React Cheatsheet Preview](https://i.imgur.com/opqHZJx.png)](https://www.codecademy.com/learn/react-101/modules/react-101-jsx-u/cheatsheet)

[Source : Codecademy](https://www.codecademy.com/)

<cta-button text="View" link="https://www.codecademy.com/learn/react-101/modules/react-101-jsx-u/cheatsheet"></cta-button>

</br>

<cta-button text="More Cheatsheets On React.js" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#reactjs"></cta-button>


## Vue.js

### Vue Essential Cheatsheet 

[![Vue Essential Cheatsheet Preview](https://i.imgur.com/5UtdOZp.png)](https://www.vuemastery.com/pdf/Vue-Essentials-Cheat-Sheet.pdf)

[Source: VueMastery](https://www.vuemastery.com/)

<cta-button text="View" link="https://www.vuemastery.com/pdf/Vue-Essentials-Cheat-Sheet.pdf"></cta-button>


### Vue.js cheatsheet

[![Vue.js cheatsheet Preview](https://i.imgur.com/Mi9qblk.png)](https://devhints.io/vue)

[Source: devhints.io](https://devhints.io/)

<cta-button text="View" link="https://devhints.io/vue"></cta-button>

### Vue cheat sheet

[![Vue cheat sheet Preview](https://i.imgur.com/tE8XNbq.png)](https://github.com/dekadentno/vue-cheat-sheet)

[Source: dekadentno from github](https://github.com/dekadentno)

<cta-button text="View" link="https://github.com/dekadentno/vue-cheat-sheet"></cta-button>

### The Vue.js Cheat Sheet

[![The Vue.js Cheat Sheet Preview](https://i.imgur.com/PWbaLhy.png)](https://flaviocopes.com/vue-cheat-sheet/)

[Source: flaviocopes.com](https://flaviocopes.com/)

<cta-button text="View" link="https://flaviocopes.com/vue-cheat-sheet/"></cta-button>


### Ultimate Vue.js cheat sheet

[![ Ultimate Vue.js cheat sheet Preview](https://i.imgur.com/MOms2rx.png)](https://eric-the-coder.com/ultimate-vuejs-2021-cheat-sheet)

[Source: eric-the-coder.com](https://eric-the-coder.com/)

<cta-button text="View" link="https://eric-the-coder.com/ultimate-vuejs-2021-cheat-sheet"></cta-button>


### Vue 3 Cheat Sheet

[![Vue 3 Cheat Sheet Preview](https://i.imgur.com/uUElOz0.png)](https://dev.to/adnanbabakan/vue-3-cheat-sheet-2mh3)

[Source: Adnan Babakan](https://dev.to/adnanbabakan)

<cta-button text="View" link="https://dev.to/adnanbabakan/vue-3-cheat-sheet-2mh3"></cta-button>





</br>

<cta-button text="More Cheatsheets On Vue.js" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#vuejs"></cta-button>

