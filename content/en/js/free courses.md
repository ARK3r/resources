---
title: Free Courses
description: We're curating useful JavaScript courses from around the Internet to help you on your software journey!
position: 3009
category: Javascript
---


## Next.js Crash Course 2021

In this video you will learn the fundamentals of Next.js such as SSR & SSG, routing, data fetching, apis and more.

### Timestamps:
- 0:00 - Intro & Slides
- 6:52 - Getting Setup with create-next-app
- 8:23 - Files & Folders
- 11:37 - Pages & Routing
- 13:14 - Head
- 16:05 - Layouts & CSS Modules
- 20:56 - Nav Component & Link
- 23:34 - Create a Header
- 25:05 - Styled JSX
- 27:46 - Custom Document
- 31:16 - Data Fetching
- 32:02 - getStaticProps()
- 33:58 - Showing Data
- 40:15 - Nested Routing
- 42:46 - getServerSideProps()
- 46:00 - getStaticPaths()
- 49:47 - Export a Static Website
- 53:18 - API Routes
- 59:24 - Using the API Data
- 1:03:48 - Custom Meta Component



<youtube-video id="mTz0GXj8NN0"> </youtube-video>

