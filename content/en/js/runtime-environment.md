---
title: Runtime Environments
description: We're curating useful resources about Javascript runtime environments from around the Internet to help you on your software journey!
position: 3001
category: Javascript
---

## Node.Js Begineer's Guide

Node.js is an open-source and cross-platform JavaScript runtime environment. This video will help you to build a fullstack web app and deploy it to a cloud server.

#### Video's Content

1. What is Node?
2. How do you install Node?
3. Hello World
4. Know the Runtime
5. Events
6. File System
7. Modules
8. Build & Deploy

<youtube-video id="ENrzD9HAZK4?list=PL0vfts4VzfNiq0-fXbVVdnngU1Ur2SzyZ"> </youtube-video>

## Deno - Server Side Javascript

Deno is a brand new JavaScript runtime, similar to Node.js. It is used to develop secure server-side apps with built-in TypeScript support, ES modules, and promise-based APIs.

<youtube-video id="F0G9lZ7gecE?list=PL0vfts4VzfNiq0-fXbVVdnngU1Ur2SzyZ"> </youtube-video>

#### Difference between Node.js and Deno

Both Node. js and Deno use the same JavaScript engine, Google's V8,The only difference that could potentially impact performance is the fact that Deno is built on Rust and Node. js on C++.

<youtube-video id="asEzNp_Y-O8"> </youtube-video>

## Electron JS

Electron.js is a runtime framework that allows the user to create desktop-suite applications with HTML5, CSS, and JavaScript.It is basically a blend of two incredibly popular technologies: Node.JS and Chromium.

In this video, you'll get an overview of how to build your first native desktop app (MacOS, Windows, Linux) with Electron JS.

<youtube-video id="m3OjWNFREJo?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>
