---
title: Books
description: We're curating useful Javascript books from around the Internet to help you on your software journey!
position: 3007
category: Javascript
---

## JavaScript

### Designing Scalable JavaScript Applications 

<book-cover link="https://www.manning.com/books/designing-scalable-javascript-applications" img-src="https://i.imgur.com/mYXuAHL.jpg" alt="Book cover for Designing Scalable JavaScript Applications "> </book-cover>

*Designing Scalable JavaScript Applications* helps you start thinking about which tools and frameworks you’ll use and which design patterns you’ll implement. This book brings together excerpts from four different Manning titles selected by Emmit Scott, the author of SPA Design and Architecture. These chapters are great starting points for understanding how to build better JavaScript applications. They introduce some fundamental concepts for creating clean, loosely coupled code, and show you how to make your development process more productive and efficient.

<cta-button text="Complete Book" link="https://www.manning.com/books/designing-scalable-javascript-applications"></cta-button>


### Building Front-End Web Apps with Plain JavaScript 

<book-cover link="https://web-engineering.info/JsFrontendApp-Book" img-src="https://i.imgur.com/zb6ajRE.jpg" alt="Book cover for Building Front-End Web Apps with Plain JavaScript "> </book-cover>

This book shows how to build front-end web applications with plain JavaScript, not using any (third-party) framework or library. A front-end web application can be provided by any web server, but it is executed on the user's computer device (smartphone, tablet or notebook), and not on the remote web server. Typically, but not necessarily, a front-end web application is a single-user application, which is not shared with other users.

<cta-button text="Complete Book" link="https://web-engineering.info/JsFrontendApp-Book"></cta-button>


### Exploring ES6 

<book-cover link="https://exploringjs.com/es6/" img-src="https://i.imgur.com/iz5U6Oy.png" alt="Book cover for Exploring ES6 "> </book-cover>

This book covers ES6 with three levels of detail:

**Quick start:** Begin with the chapter “Core ES6 features”. Additionally, almost every chapter starts with a section giving an overview of what’s in the chapter. The last chapter collects all of these overview sections in a single location.

**Solid foundation:** Each chapter always starts with the essentials and then increasingly goes into details. The headings should give you a good idea of when to stop reading, but I also occasionally give tips in sidebars w.r.t. how important it is to know something.

**In-depth knowledge:** Read all of a chapter, including the in-depth parts.

<cta-button text="Complete Book" link="https://exploringjs.com/es6/"></cta-button>

</br>

<cta-button text="More Books On JavaScript" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#javascript"></cta-button>

<cta-button text="More Resources On JavaScript" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#javascript"></cta-button>


## AngularJS

### AngularJS - Step by Logical Step - Nicholas Johnson

<book-cover link="http://nicholasjohnson.com/blog/angularjs-step-by-logical-step/" img-src="https://i.imgur.com/yaJEdPp.png" alt="Book cover for AngularJS - Step by Logical Step"> </book-cover>

This book is your guide to Angular. Angular is a front-end JavaScript framework that helps you build single page web applications (SPAs). It comes packaged as a single JavaScript file which you include on your web page. It's purely front end, and says nothing about your server. Your server only needs to be able to ship out JSON to be able to talk to Angular.

<cta-button text="Complete Book" link="http://nicholasjohnson.com/blog/angularjs-step-by-logical-step/"></cta-button>

### AngularJS Succinctly 

<book-cover link="https://www.syncfusion.com/succinctly-free-ebooks/angularjs" img-src="https://i.imgur.com/OeZwAdY.jpg" alt="Book cover for AngularJS Succinctly"> </book-cover>

Author Frederik Dietz uses AngularJS Succinctly to outline common tasks and challenges for developers using Angular.js. With his help, novices and experts alike will find a reference that clearly outlines a variety of challenges, their solutions, and technical explanations for how the challenges are overcome. Whether you are a novice looking to understand Angular.js or an expert seeking a reference guide, AngularJS Succinctly is indispensable!

<cta-button text="Complete Book" link="https://www.syncfusion.com/succinctly-free-ebooks/angularjs"></cta-button>


</br>

<cta-button text="More Books On AngularJS" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#angularjs"></cta-button>

<cta-button text="More Resources On AngularJS" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#angularjs"></cta-button>


## Backbone.js

### Backbonejs Tutorials

<book-cover link="https://cdnjs.com/libraries/backbone.js/tutorials/" img-src="https://i.imgur.com/0gG7oFe.png" alt="Book cover for Backbonejs Tutorials"> </book-cover>

Backbone.js gives structure to web applications by providing models with key-value binding and custom events, collections with a rich API of enumerable functions, views with declarative event handling, and connects it all to your existing API over a RESTful JSON interface.

<cta-button text="Complete Book" link="https://cdnjs.com/libraries/backbone.js/tutorials/"></cta-button>

</br>

<cta-button text="More Books On Backbone.js" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#backbonejs"></cta-button>


## Booty5.js

### The Booty5 HTML5 Game Maker Manual

<book-cover link="http://booty5.com/booty5-free-html-game-maker-e-book-manual/" img-src="https://i.imgur.com/H3cn4sN.jpg" alt="Book cover for The Booty5 HTML5 Game Maker Manual"> </book-cover>

This e-book currently covers the entire Booty HTML5 game engine and game editor.

Booty5 is a combination of WYSIWYG game editor and game engine that targets the HTML5 platform for desktops such as Windows, Mac and Linux and mobile devices such as iOS, Android, Windows Phone 8, Blackberry and more.

Booty5 enables rapid game development and testing using an intuitive super quick WYSIWYG interface that can easily handle small to super massive projects.

<cta-button text="Complete Book" link="http://booty5.com/booty5-free-html-game-maker-e-book-manual/"></cta-button>

</br>

<cta-button text="More Books On Booty5.js" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#booty5js"></cta-button>


## D3.js

### D3 Tips and Tricks

<book-cover link="https://leanpub.com/D3-Tips-and-Tricks/read" img-src="https://i.imgur.com/kuEyvoJ.jpg" alt="Book cover for D3 Tips and Tricks "> </book-cover>

D3 is all about helping you to take information and make it more accessible to others via a web browser.

It’s a JavaScript library. That means that it’s a tool that can be used in conjunction with other tools to get a job done. Those other tools are mainly HTML and CSS (amongst others) but you don’t need to know too much about either to use D3 (although it will help).

It’s an open framework, which means that there are no hidden mysteries about how it does its magic and it allows others to contribute to a constant cycle of improvement

<cta-button text="Complete Book" link="https://leanpub.com/D3-Tips-and-Tricks/read"></cta-button>


### Interactive Data Visualization with D3

<book-cover link="https://alignedleft.com/tutorials/d3" img-src="https://i.imgur.com/S9tr1FI.jpg" alt="Book cover for Interactive Data Visualization with D3"> </book-cover>

Create and publish your own interactive data visualization projects on the webâ??even if you have little or no experience with data visualization or web development. Itâ??s inspiring and fun with this friendly, accessible, and practical hands-on introduction. This fully updated and expanded second edition takes you through the fundamental concepts and methods of D3, the most powerful JavaScript library for expressing data visually in a web browser.

Ideal for designers with no coding experience, reporters exploring data journalism, and anyone who wants to visualize and share data, this step-by-step guide will also help you expand your web programming skills by teaching you the basics of HTML, CSS, JavaScript, and SVG

<cta-button text="Complete Book" link="https://alignedleft.com/tutorials/d3"></cta-button>

</br>

<cta-button text="More Books On D3.js" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#d3js"></cta-button>


## Dojo

### Dojo: The Definitive Guide 

<book-cover link="https://www.oreilly.com/library/view/dojo-the-definitive/9780596516482/" img-src="https://i.imgur.com/NzkCH5L.jpg" alt="Book cover for Dojo: The Definitive Guide"> </book-cover>

**Dojo: The Definitive Guide** demonstrates how to tame Dojo's extensive library of utilities so that you can build rich and responsive web applications like never before. Dojo founder Alex Russell gives a foreword that explains the "why" of Dojo and of this book.

Dojo provides an end-to-end solution for development in the browser, including everything from the core JavaScript library and turnkey widgets to build tools and a testing framework. Its vibrant open source community keeps adding to Dojo's arsenal, and this book provides an ideal companion to Dojo's official documentation.

<cta-button text="Complete Book" link="https://www.oreilly.com/library/view/dojo-the-definitive/9780596516482/"></cta-button>

</br>

<cta-button text="More Books On Dojo" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#dojo"></cta-button>


## Electron

### Electron Succinctly

<book-cover link="https://www.syncfusion.com/succinctly-free-ebooks/electron-succinctly" img-src="https://i.imgur.com/3YuGmso.jpg" alt="Book cover for Electron Succinctly"> </book-cover>

Developing web apps can be made more difficult by the fact that they do not easily or natively run in desktop environments. This can make a variety of development skills useless, reducing the amount of expertise even the best developers can bring to a project and increasing the workload for producing cross-platform apps. But with the Electron framework, web apps can be brought seamlessly to desktop environments, opening up new avenues for developers. In Electron Succinctly, author [Ed Freitas](https://www.edfreitas.me/) serves as a guide to getting started with Electron.

<cta-button text="Complete Book" link="https://www.syncfusion.com/succinctly-free-ebooks/electron-succinctly"></cta-button>

</br>

<cta-button text="More Books On Electron" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#electron"></cta-button>


## Elm

### An Introduction to Elm 

<book-cover link="https://guide.elm-lang.org/" img-src="https://i.imgur.com/MCdhtna.png" alt="Book cover for An Introduction to Elm "> </book-cover>

Elm is a functional language that compiles to JavaScript. It helps you make websites and web apps. It has a strong emphasis on simplicity and quality tooling.

This guide will:

- Teach you the fundamentals of programming in Elm.
- Show you how to make interactive apps with The Elm Architecture.
- Emphasize principles and patterns that generalize to programming in any language.

<cta-button text="Complete Book" link="https://guide.elm-lang.org/"></cta-button>

### Elm Programming Language 

<book-cover link="https://en.wikibooks.org/wiki/Elm_programming_language" img-src="https://i.imgur.com/gUmGcGI.png" alt="Book cover for Elm Programming Language"> </book-cover>

Elm is a functional programming language for declaratively creating web browser based graphical user interfaces.

Elm uses the Functional Reactive Programming style and purely functional graphical layout to build user interface without any destructive updates.

<cta-button text="Complete Book" link="https://en.wikibooks.org/wiki/Elm_programming_language"></cta-button>

</br>

<cta-button text="More Books On Elm" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#elm"></cta-button>


## Ember.js

### Building a complex web application with Ember.js Octane

<book-cover link="https://yoember.com/" img-src="https://i.imgur.com/OEFPpwC.jpg" alt="Book cover for Building a complex web application with Ember.js Octane"> </book-cover>

This is an Ember.js tutorial from the absolute beginner level. This tutorial is continuously improved and updated, it uses Ember Octane (v3.19).

<cta-button text="Complete Book" link="https://yoember.com/"></cta-button>


### Ember.js - Getting started

<book-cover link="https://guides.emberjs.com/release/" img-src="https://i.imgur.com/dp6kvbu.png" alt="Book cover for Ember.js - Getting started"> </book-cover>

This documentation will take you from total beginner to Ember expert.

With the plethora of libraries readily available for front-end development, sometimes it can be a little confusing to work with a front-end framework like Ember.js, where everything you need to build an application is already included. To that end, each part of the guides has been segmented out so you can focus on just the part you want to work with. This should also make it faster for you to find what you need!

<cta-button text="Complete Book" link="https://guides.emberjs.com/release/"></cta-button>

</br>

<cta-button text="More Books On Ember.js" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#emberjs"></cta-button>


## Express.js

### Express.js Guide

<book-cover link="https://web.archive.org/web/20140621124403/https://leanpub.com/express/read" img-src="https://i.imgur.com/P1mUaQR.png" alt="Book cover for Express.js Guide"> </book-cover>

This book is for people fluent in programming and front-end JavaScript. To get the most benefits, readers must be familiar with basic Node.js concepts like process and global, and know core modules, including stream, cluster, and buffer.

If you’re thinking about starting a Node.js project, or about rewriting an existing one, and your weapon of choice is Express.js — this guide is for you! It will answer most of your “how” and “why” questions.

<cta-button text="Complete Book" link="https://web.archive.org/web/20140621124403/https://leanpub.com/express/read"></cta-button>

</br>

<cta-button text="More Books On Express.js" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#expressjs"></cta-button>


## Fastify

### Fastify - Latest Documentation 

<book-cover link="https://www.fastify.io/docs/latest/" img-src="https://i.imgur.com/WSzzMS6.png" alt="Book cover for Fastify - Latest Documentation"> </book-cover>

Fastify is a web framework highly focused on providing the best developer experience with the least overhead and a powerful plugin architecture, inspired by Hapi and Express. 

The documentation for Fastify is split into two categories:
- **Reference documentation:** The reference documentation utilizes a very formal style in an effort to document Fastify's API and implementation details thoroughly for the developer who needs such.
- **Guides:** The guides category utilizes an informal, educational, style as a means to introduce newcomers to core, and advanced, Fastify concepts.

<cta-button text="Complete Book" link="https://www.fastify.io/docs/latest/"></cta-button>

</br>

<cta-button text="More Books On Fastify" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#fastify"></cta-button>


## Ionic

### Ionic 4 Succinctly

<book-cover link="https://www.syncfusion.com/succinctly-free-ebooks/ionic-4-succinctly" img-src="https://i.imgur.com/fc6d7jT.png" alt="Book cover for Ionic 4 Succinctly"> </book-cover>

Ionic 4 takes the original Ionic toolkit from a mobile-centric framework based on Angular to a powerful, web-based UI design system and app-development toolset that is JavaScript-framework agnostic. The increased performance of Ionic 4 components makes the framework ideal for developing progressive web apps (PWAs), which are in high demand and popularity these days.

With Ionic 4 Succinctly, author [Ed Freitas](https://www.edfreitas.me/) will focus on progressive web apps and show you how you can use Ionic 4 to build one, using Vue as the JavaScript framework.

<cta-button text="Complete Book" link="https://www.syncfusion.com/succinctly-free-ebooks/ionic-4-succinctly"></cta-button>

</br>

<cta-button text="More Books On Ionic" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#ionic"></cta-button>


## jQuery

### jQuery Succinctly, Syncfusion


<book-cover link="https://www.syncfusion.com/succinctly-free-ebooks/jquery" img-src="https://i.imgur.com/dI5DNxg.jpg" alt="Book cover for jQuery Novice to Ninja "> </book-cover>

This book is intended for two types of readers. The first is someone who has read introductory material on jQuery and is looking for the next logical step. The second type of reader is a JavaScript developer, already versed in another library, now trying to quickly learn jQuery

<cta-button text="Complete Book" link="https://www.syncfusion.com/succinctly-free-ebooks/jquery"></cta-button>


### jQuery Novice to Ninja 

<book-cover link="chrome-extension://efaidnbmnnnibpcajpcglclefindmkaj/http://mediatheque.cite-musique.fr/MediaComposite/Debug/Dossier-Orchestre/ressources/jQuery.Novice.to.Ninja.2nd.Edition.pdf" img-src="https://i.imgur.com/6EIzYdb.png" alt="Book cover for "> </book-cover>

If you’re a front-end web designer looking to add a dash of cool interactivity to your sites, and you’ve heard all the buzz surrounding jQuery and want to find out what the fuss is about, this book will put you on the right track.

By the end of this book, you’ll be able to take your static HTML and CSS web pages and bring them to life with a bit of jQuery magic. You’ll learn how to select elements on the page, move them around, remove them entirely, add new ones with Ajax, animate them and bend HTML and CSS to your will.

<cta-button text="Complete Book" link="chrome-extension://efaidnbmnnnibpcajpcglclefindmkaj/http://mediatheque.cite-musique.fr/MediaComposite/Debug/Dossier-Orchestre/ressources/jQuery.Novice.to.Ninja.2nd.Edition.pdf"></cta-button>


</br>

<cta-button text="More Books On jQuery" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#jquery"></cta-button>

<cta-button text="More Resources On jQuery" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#jquery"></cta-button>


## meteor

### Your First Meteor Application, A Complete Beginner’s Guide to the Meteor JavaScript Framework

<book-cover link="http://meteortips.com/first-meteor-tutorial/" img-src="https://i.imgur.com/ubdcfU5.jpg" alt="Book cover for Your First Meteor Application, A Complete Beginner’s Guide to the Meteor JavaScript Framework"> </book-cover>

**Your First Meteor Application** is a free, online book about the Meteor JavaScript framework that will help you build your first real-time web application with Meteor in a matter of hours.

<cta-button text="Complete Book" link="http://meteortips.com/first-meteor-tutorial/"></cta-button>

</br>

<cta-button text="More Books On meteor" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#meteor"></cta-button>


## Next.js

### The Next.js Handbook

<book-cover link="https://flaviocopes.com/page/nextjs-handbook/" img-src="https://i.imgur.com/zy4OwcR.jpg" alt="Book cover for The Next.js Handbook"> </book-cover>

This tutorial will help you to quickly learn Next.js and get familiar with how it works.

It's ideal for you if you have zero to little knowledge of Next.js, you have used React in the past, and you are looking forward diving more into the React ecosystem, in particular server-side rendering.

<cta-button text="Complete Book" link="https://flaviocopes.com/page/nextjs-handbook/"></cta-button>

</br>

<cta-button text="More Books On Next.js" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#nextjs"></cta-button>


## Node.js

### How To Code in Node.js

<book-cover link="https://www.digitalocean.com/community/books/how-to-code-in-node-js-ebook" img-src="https://i.imgur.com/KhW1wn0.png" alt="Book cover for How To Code in Node.js"> </book-cover>

In this book, you will go through exercises to learn the basics of how to code in Node.js, gaining skills that apply equally to back-end and full stack development in the process.

By the end of this book you will be able to write programs that leverage Node’s asynchronous code execution capabilities, complete with event emitters and listeners that will respond to user actions. Along the way you will learn how to debug Node applications using the built-in debugging utilities, as well as the Chrome browser’s DevTools utilities. You will also learn how to write automated tests for your programs to ensure that any features that you add or change function as you expect.

<cta-button text="Complete Book" link="https://www.digitalocean.com/community/books/how-to-code-in-node-js-ebook"></cta-button>

### From Containers to Kubernetes with Node.js

<book-cover link="https://www.digitalocean.com/community/books/from-containers-to-kubernetes-with-node-js-ebook" img-src="https://i.imgur.com/1GtP9kA.png" alt="Book cover for From Containers to Kubernetes with Node.js"> </book-cover>

This book is designed to introduce you to using containers and Kubernetes for full-stack development. You’ll learn how to develop a full-stack application using Node.js and MongoDB and how to manage them; first with Docker, then with Docker Compose, and finally with Kubernetes

<cta-button text="Complete Book" link="https://www.digitalocean.com/community/books/from-containers-to-kubernetes-with-node-js-ebook"></cta-button>

### Full Stack JavaScript: Learn Backbone.js, Node.js and MongoDB 

<book-cover link="https://github.com/azat-co/fullstack-javascript" img-src="https://i.imgur.com/9fc7Zs4.jpg" alt="Book cover for Full Stack JavaScript: Learn Backbone.js, Node.js and MongoDB"> </book-cover>

This is a hands-on book which introduces you to agile JavaScript web and mobile software development using the latest cutting-edge front-end and back-end technologies including: Node.js, MongoDB, Backbone.js, Parse.com, Heroku and Windows Azure.

<cta-button text="Complete Book" link="https://github.com/azat-co/fullstack-javascript"></cta-button>


</br>

<cta-button text="More Books On Node.js" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#nodejs"></cta-button>


## React 

### How To Code in React.js 

<book-cover link="https://www.digitalocean.com/community/books/how-to-code-in-react-js-ebook" img-src="https://i.imgur.com/hVPRHaS.jpg" alt="Book cover for How To Code in React.js "> </book-cover>

This book is an introduction to React that works from the foundations upward. Each chapter takes you a little deeper into the React ecosystem, building on your previous knowledge. Along the way, you’ll learn how to maintain internal state, pass information between parts of an application, and explore different options for styling your application. 

Every chapter is self contained, so you can jump between chapters or skip whole sections. The book is designed for you to take a concept and explore it by building a small project that mirrors what you will encounter in everyday development using React.

<cta-button text="Complete Book" link="https://www.digitalocean.com/community/books/how-to-code-in-react-js-ebook"></cta-button>

### React in patterns

<book-cover link="https://krasimir.gitbooks.io/react-in-patterns/content/" img-src="https://i.imgur.com/UUv046M.jpg" alt="Book cover for React in patterns"> </book-cover>

A book about common design patterns used while developing with React. It includes techniques for composition, data flow, dependency management and more.

<cta-button text="Complete Book" link="https://krasimir.gitbooks.io/react-in-patterns/content/"></cta-button>


</br>

<cta-button text="More Books On React " link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#react"></cta-button>

<cta-button text="More Resources On React " link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#react"></cta-button>


## React Native

### The Ultimate Guide to React Native Optimization 

<book-cover link="https://www.callstack.com/blog/download-the-ultimate-guide-to-react-native-optimization-ebook" img-src="https://i.imgur.com/at53beZ.png" alt="Book cover for The Ultimate Guide to React Native Optimization"> </book-cover>

This guide is a collection of the most relevant and effective tips, tricks, tactics, and solutions for optimizing React Native apps. They are based on the experienced senior developers and Core Contributors to React Native.

<cta-button text="Complete Book" link="https://www.callstack.com/blog/download-the-ultimate-guide-to-react-native-optimization-ebook"></cta-button>

### React Native Express

<book-cover link="https://www.reactnative.express/" img-src="https://i.imgur.com/g3fRFW0.png" alt="Book cover for React Native Express"> </book-cover>

React Native is a JavaScript framework for building cross-platform apps. This guide covers everything you need to know to start developing React Native apps.

<cta-button text="Complete Book" link="https://www.reactnative.express/"></cta-button>

</br>

<cta-button text="More Books On React Native" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#react-native"></cta-button>


## Redux

### Full-Stack Redux Tutorial

<book-cover link="https://teropa.info/blog/2015/09/10/full-stack-redux-tutorial.html" img-src="https://i.imgur.com/xATQCvc.jpg" alt="Book cover for Full-Stack Redux Tutorial"> </book-cover>

This tutorial will guide you through building a full-stack Redux and Immutable-js application from scratch. We'll go through all the steps of constructing a Node+Redux backend and a React+Redux frontend for a real-world application, using test-first development.


<cta-button text="Complete Book" link="https://teropa.info/blog/2015/09/10/full-stack-redux-tutorial.html"></cta-button>

</br>

<cta-button text="More Books On Redux" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#redux"></cta-button>

<cta-button text="More Resources On Redux" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#redux"></cta-button>


## Svelte

### Svelte Tutorial 

<book-cover link="https://svelte.dev/tutorial/basics" img-src="https://i.imgur.com/BZ6Z1he.jpg" alt="Book cover for Svelte Tutorial"> </book-cover>

This tutorial will teach you everything you need to know to build fast, small web applications easily.

<cta-button text="Complete Book" link="https://svelte.dev/tutorial/basics"></cta-button>

</br>

<cta-button text="More Books On Svelte" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#svelte"></cta-button>


## Vue.js

### Learning Vue.js

<book-cover link="https://riptutorial.com/Download/vue-js.pdf" img-src="https://i.imgur.com/w1DnF12.png" alt="Book cover for  Learning Vue.js"> </book-cover>

It is an unofficial and free Vue.js ebook created for educational purposes. All the content is extracted from Stack Overflow Documentation, which is written by many hardworking individuals at Stack Overflow. It is neither affiliated with Stack Overflow nor official Vue.js.

<cta-button text="Complete Book" link="https://riptutorial.com/Download/vue-js.pdf"></cta-button>

### 30 Days Of Vue 

<book-cover link="https://www.newline.co/30-days-of-vue" img-src="https://i.imgur.com/mJwUKPm.jpg" alt="Book cover for 30 Days Of Vue "> </book-cover>

In this book you will learn everything you need to know to work with Vue. From the very beginning through topics like the Vue Instance, Components, and even Testing.

<cta-button text="Complete Book" link="https://www.newline.co/30-days-of-vue"></cta-button>

</br>

<cta-button text="More Books On Vue.js" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#vuejs"></cta-button>


