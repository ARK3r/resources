---
title: Software Leadership
description: We're compiling a library of useful software leadership resources from around the Internet to help you on your journey!
position: 1001
category: Entrepreneurship
---

> We're compiling a library of useful software leadership resources from around the Internet to help you on your journey!

## Refactoring by Luca Rossi

![Browsers Preview](https://i.imgur.com/ZyqyHZg.jpg)

Refactoring is a weekly column about making great software, working with people, and personal growth.

<cta-button  link="https://refactoring.fm/archive?sort=top" text="View Articles" > </cta-button>
