---
title: Pitching
description: We're compiling a library of useful resources about pitching your idea as an entrepreneur!
position: 1000
category: Entrepreneurship
---

## Writing Your Pitch Narrative

![Writing Your Pitch Narrative Preview](https://underscore.vc/wp-content/uploads/2021/03/Pitch-Narrative-Template.png)

Remember the last dry presentation you heard? A monotonous voice. Rambling thoughts. Confusing transitions.

Crafting a great pitch narrative is no easy feat. When there’s so much information you could include, it can be all too easy to throw a number on a slide and say, “Here’s our market size.” Or add a screenshot and note, “Here’s our product.” But that’s when your pitch can get dull (and doesn’t stand out).

To prevent your pitch from turning into a snooze fest, tell it as a compelling story. Who is your protagonist? What challenges are they overcoming? How will they accomplish this? Where’s the emotion?

<cta-button  link="https://underscore.vc/startupsecrets/pitch-narrative-template/" text="Read Article"></cta-button>

## How to Create a Pitch Deck for Investors

In this video, Slidebean's Kaya dig deep into the standard pitch deck outline most companies are using these days, and they give you some guidance as to what information and in what format should be included on your document.

<youtube-video id="SB16xgtFmco"> </youtube-video>

<br/>

## How to Give a Perfect Investor Pitch

![How to Give a Perfect Investor Pitch Preview](https://underscore.vc/wp-content/uploads/2020/06/Underscore_How_to_Give_a_Perfect_Investor_Pitch.png)

You have to build your pitch on the reality of your business plan. Ultimately, it’s not about the slides you show or the words you use. It’s about you, your team, your value proposition, and your business strategy.

<cta-button  link="https://underscore.vc/startupsecrets/how-to-pitch-to-investors/" text="Read Article"></cta-button>

<iframe allowtransparency="true" title="Wistia video player" allowFullscreen frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" src="https://fast.wistia.net/embed/iframe/zolk1m0ezd" width="100%" height="350px"></iframe>

## 8 red flags when pitching investors

Raising venture capital is no easy task; many variables need to come into play and align for a round of funding to happen. Slidebean has extensively covered what a pitch deck should have and what your financials should answer, and they've discovered the following deal-breakers:

- Incorrect founding team
- Baggage
- Raising money to survive 
- Crazy founder salaries 
- Vanity metrics
- Not understanding your KPIs
- Distractions
- Not enough money 

<youtube-video id="FnkTO5Ml34U"> </youtube-video>

## Pitch Deck Mistakes: Go-To-Market Strategy

Go-to-Market Strategy is a super important item investors really want to understand, yet most startups don’t really know what the term means. They struggle to describe their go-to-market strategy in a clear and focused manner. Usually, when startups are asked about their go-to-market strategy they’d answer something like: “we’re doing direct sales” or “we’ll do online sales.” That’s not a go-to-market strategy. Those are sales tactics.

<youtube-video id="o0ygMSNZrHY"> </youtube-video>

## How to estimate market size? (TAM or Total Addressable Market)

This video will help you learn basic definition of TAM, what are the most common mistakes made while calculating and presenting this to investors, and how to estimate it properly?

<youtube-video id="M_RMTC2YmXY"> </youtube-video>
