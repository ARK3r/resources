---
title: Videos
description: We're compiling a library of useful videos about entrepreneurship from around the Internet to help you on your software journey!
position: 1002
category: Entrepreneurship
---

> We're compiling a library of useful entrepreneurial videos from around the Internet to help you on your software journey!

## Startup Secrets by Harvard iLab

![Startup Secrets by Harvard iLab Preview](https://img.youtube.com/vi/EYJeGYboPnw/maxresdefault.jpg)

<cta-button  link="https://www.youtube.com/embed/SduKH5rf850?list=PLzKJi2GjpkEHB0CuR7JP-rARjkqnzRM2x" text="View Playlist"></cta-button>

## Billion Dollar Startups are Better, Faster, & Cheaper

This video is for founders who are not thinking about their startups in a way that will convince people to switch to use their products or services and if that doesn't seem like it will happen, they won't be able to build teams, or raise money either. So the self-fulfilling prophecy cannot work. Can you describe what you are doing in classic better, faster, cheaper terms? 

Facebook wasn't the first social network. So maybe being first doesn't matter. But, being better, faster, or cheaper? That matters a lot.

<youtube-video id="opkHJLVAM4A"></youtube-video>


## 6 Skills for Successful Startup Founders

You can't guarantee success, but you can increase the chances of it by working on these six skills for startup founders. 

<youtube-video id="WK_XwjhJl3k"></youtube-video>

## Build Your Way to Getting Rich - Steve Jobs' Mindfulness Hack Explained

When we feel alone and powerless, we think that getting rich is going to help. But for Garry Tan, he had to get beyond his complaint-filled, negative mindset to actually build. Here's how you can avoid the his mistakes.

<youtube-video id="Q4kPDixyGQY"></youtube-video>

## How YOU can Invent the Future in 2022 -- like the Wright Brothers did in 1903.

In this video, Garry Tan is going to talk about what it took to push humanity forward, by looking into the age of aviation: a time that brought the world together and changed the course of humanity through technology. What seemed impossible made the crossover to possible.

<youtube-video id="Qp1pNlLCta4"></youtube-video>

## Quit and join that risky tech startup? A guide to learning, earning & minimizing regret at startups

Should you quit your job? What skills do you want to acquire? How do you evaluate how much you'll actually earn? If you're great at what you do, it turns out that the only kind of risk is not taking risk at all.

This is a quick guide on how to think through different opportunities that come your way. Learning early on is more important, but earning is absolutely something you need to focus on mid-career. 

<youtube-video id="RhYZECR2Ru8"></youtube-video>

## Billion Dollar Startup Ideas

What makes you new, different, and unique in the world will often be the exact thing that makes you succeed. Embrace your differences, and you’ll find something amazing in there. Learn how founders like Steve Jobs, Bill Gates, Ryan Petersen of Flexport, and Jack Conte of Patreon did exactly that: learn things from unique personal experiences that put them on the right path. 

And if you can do that, you avoid infinite competition. You can make something truly awesome. 

<youtube-video id="3YKNr-LiblI"></youtube-video>

## Get Users to Do What You Want | The Future of Customer Service & Onboarding Customers with Cohere

Both startups and corporate giants alike wish they could be next to users in the moment when they are going through a problem. Cohere lets you do actually do that and avoid losing that person forever. 

This video will help you learn about the future of user onboarding and customer service with Cohere.

<youtube-video id="KD2bkgoxSSI"></youtube-video>

## My $200 Million Startup Mistake

In this video Garry Tan talks about a mistake he made when he was offered to work in a startup. 

<youtube-video id="dtnG0ELjvcM"></youtube-video>

