---
title: Developer Roadmaps
description:  We're compiling a library of useful developers' roadmaps from around the Internet to help you on your software journey!
position: 6
category: Overview
---

## Frontend Roadmap

Step by step guide to become a modern frontend developer.

[![Frontend Roadmap](https://i.imgur.com/qXBlmnb.jpg)](https://roadmap.sh/frontend)

<cta-button text="View Roadmap" link="https://roadmap.sh/frontend"></cta-button>

## Backend Roadmap

Step by step guide to become a modern backend developer.

[![Backend Roadmap](https://i.imgur.com/PsGEnPQ.jpg)](https://roadmap.sh/backend)

<cta-button text="View Roadmap" link="https://roadmap.sh/backend"></cta-button>

## DevOps Roadmap

Step by step guide for DevOps, SRE or any other Operations Role.

[![DevOps Roadmap](https://i.imgur.com/xaG41iE.png)](https://roadmap.sh/devops)

<cta-button text="View Roadmap" link="https://roadmap.sh/devops"></cta-button>

## React Roadmap

A detailed guide of everything that is there to learn about React and their ecosystem.

[![React Roadmap](https://i.imgur.com/GeC5NWN.png)](https://roadmap.sh/react)

<cta-button text="View Roadmap" link="https://roadmap.sh/react"></cta-button>

## Angular Roadmap

A detailed guide of everything that is there to learn about Angular and their ecosystem.

[![Angular Roadmap](https://i.imgur.com/eEUnRkr.png)](https://roadmap.sh/angular)

<cta-button text="View Roadmap" link="https://roadmap.sh/angular"></cta-button>

## Android Roadmap

Step by step guide to become an Android developer.

[![Android Roadmap](https://i.imgur.com/24hU80G.png)](https://roadmap.sh/android)

<cta-button text="View Roadmap" link="https://roadmap.sh/android"></cta-button>

## Python Roadmap

Step by step guide to become a Python developer.

[![Android Roadmap](https://i.imgur.com/klASfw8.png)](https://roadmap.sh/python)

<cta-button text="View Roadmap" link="https://roadmap.sh/python"></cta-button>

## Go Roadmap

Step by step guide to become a Go developer.

[![Go Roadmap](https://i.imgur.com/k9FQaZO.png)](https://roadmap.sh/golang)

<cta-button text="View Roadmap" link="https://roadmap.sh/golang"></cta-button>

## Java Roadmap

Step by step guide to become a Java developer.

[![Java Roadmap](https://i.imgur.com/TNMsmlO.png)](https://roadmap.sh/java)

<cta-button text="View Roadmap" link="https://roadmap.sh/java"></cta-button>

## DBA Roadmap

Step by step guide to becoming a modern PostgreSQL DB Administrator.

[![DBA Roadmap](https://i.imgur.com/lW3vcU2.png)](https://roadmap.sh/postgresql-dba)

<cta-button text="View Roadmap" link="https://roadmap.sh/postgresql-dba"></cta-button>
