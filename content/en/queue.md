---
title: Queue
description: This page contains resources submitted by our community that we will process and add to our library as needed!
category: Overview
position: 3
---

<cta-button text="Contribute to this Page" link="https://gitlab.com/-/ide/project/grey-software/resources/edit/master/-/content/en/queue.md"></cta-button>

- https://www.designsystemsforfigma.com/ (This one should have it's own page with the design systems listed + other design systems from around the internet)

- https://www.youtube.com/watch?v=NLLMwJwDgBs
- https://www.youtube.com/watch?v=V5qvWl-O-zE
- https://www.youtube.com/watch?v=i_T33FSl254
- https://www.youtube.com/watch?v=k1FSybMulVQ

- https://educationaltechnology.net/open-free-educational-resources-oer-teaching-learning/

# Resources

### Icons

- https://www.iconshock.com/freeicons
- https://feathericons.com
- https://iconduck.com
- https://iconoir.com
- https://icons.modulz.app
- https://3dicons.co
- https://icons8.com/line-awesome
- https://akaricons.com
- https://www.notion.so/Doodlicons-519314a92ed3474093a10e44946bbb72
- https://openmoji.org
- https://icons.craftwork.design

### Terminal and Command Line

- https://linux.die.net/man/1/intro
- https://www.youtube.com/watch?v=ZtqBQ68cfJc
- https://ubuntu.com/tutorials/command-line-for-beginners#1-overview
- https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Understanding_client-side_tools/Command_line
- https://docs.microsoft.com/en-us/windows/terminal/

### Web Resources

- https://css-challenges.com/
- https://andreasbm.github.io/web-skills/
- https://amplication.com/

### Resume

- https://rxresu.me/

### UX Design

- https://uxdesignweekly.com/
- https://www.nngroup.com
- https://www.uxbeginner.com/faq/
- https://growth.design/case-studies/
- https://www.casestudy.club/
- https://youtu.be/v5Q4egnlsvw

### More Design Resources

- https://www.invisionapp.com/inside-design/
- https://uxstarterpack.com/
- https://www.designresourc.es/
- https://medium.com/@catalinarusu/debunking-the-ux-myth-over-again-e05b786917ce
- https://www.thegymnasium.com/courses
- https://www.designnotes.co/
- https://www.booklets.io/
- https://www.designbetter.co/
- https://www.designlife.fm/
- https://masterdesignblog.com/
- https://scribbles.design
- https://designresourc.es/
- https://worldvectorlogo.com/
- https://logoipsum.com/
- https://www.pixsellz.io/#freebies
- https://uifreebies.net/figma
- https://www.streamlinehq.com/freebies


- 7 books for web designers :

  - Dont make me Think by Steve Krug
  - Seductive Interaction Design by Stephen P. Anderson
  - Mobile Firs by Luke Wroblewski
  - Just Enough Research by Erika Hall
  - Laws of UX by Jon Yablonski
  - Thinking with Type by Ellen Lupton
  - Tragic Design by Cynthia Savard Saucier and Jonathan Shariat

- Design Books you need to check out :
  - Josed Albers : Interaction of Colors
  - I used to be a design student
  - LogoType & Symbol
  - Punk Flyers
  - Protest : The Aesthetic of Resistance → Zurich University of the Arts

### Agencies and Studios

HUGE
[https://www.hugeinc.com/careers/jobs](https://www.hugeinc.com/careers/jobs)
Branding/Creative agency : [https://cuberto.com](https://cuberto.com/)
MullenLowe Group : [https://www.mullenlowegroup.com/](https://www.mullenlowegroup.com/)
Superhero Cheesecake : [https://superherocheesecake.com/](https://superherocheesecake.com/)[https://www.instagram.com/superherocheesecake/](https://www.instagram.com/superherocheesecake/)
Technology Humans and Taste : [https://technologyhumansandtaste.com/](https://technologyhumansandtaste.com/)
Instrument agency : [https://www.instrument.com/](https://www.instrument.com/)
Purple Rock Scissors Strategic Creative Agency : [https://purplerockscissors.com/](https://purplerockscissors.com/)
Big Human digital product studio : [https://bighuman.com/](https://bighuman.com/)
Resn Experience Design studio : [http://resn.co.nz/](http://resn.co.nz/)
Basic branding and digital agency : [https://basicagency.com/](https://basicagency.com/)
Jam3 branding agency : [https://jam3.com/](https://jam3.com/)
Active Theory creative agency : [https://activetheory.net/](https://activetheory.net/)
Unit9 production company : [https://unit9.com/](https://unit9.com/)
Upstatement Agency : [https://upstatement.com/](https://upstatement.com/)
Watson agency : [https://watson.la/](https://watson.la/)
Immersive G agency : [https://immersive-g.com/](https://immersive-g.com/)
Stink Studios advertising and digital agency : [https://www.stinkstudios.com/](https://www.stinkstudios.com/)
Defunct studio with cool site : [https://www.legworkstudio.com/](https://www.legworkstudio.com/)
Firstborn brand agency : [https://www.firstborn.com/](https://www.firstborn.com/)
Dogstudio creative studio : [https://dogstudio.co/](https://dogstudio.co/)
Kindred Studio : [https://www.kindredstudio.net/](https://www.kindredstudio.net/)
Paiheme Illustrations : [https://dribbble.com/paiheme_studio](https://dribbble.com/paiheme_studio)
Ben Mingo : [https://www.instagram.com/benmingo/](https://www.instagram.com/benmingo/)
MOUTHWASH Studio : [https://mouthwash.studio/](https://mouthwash.studio/)
Nomadic Tribe : [https://2019.makemepulse.com/](https://2019.makemepulse.com/)
20 Creative Agencies Redefining Content in NYC
[https://www.builtinnyc.com/2019/09/30/creative-agencies-nyc](https://www.builtinnyc.com/2019/09/30/creative-agencies-nyc)
