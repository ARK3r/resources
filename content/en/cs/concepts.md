---
title: Concepts
description: We're compiling a library of useful computer programming concepts from around the Internet to help you on your software journey!
position: 502
category: Computer Science
---

## 100+ Computer Science Concepts Explained

Learn the fundamentals of Computer Science with a quick breakdown of jargon that every software engineer should know. 

In this video, Fireship explains over 100 technical concepts from CS curriculums to provide a foundation for programmers.

https://www.youtube.com/watch?v=-uleG_Vecis

## Big-O Notation

Big-O notation is the language we use for talking about how long an algorithm takes to run (time complexity) or how much memory is used by an algorithm (space complexity). Big-O notation can express the best, worst, and average-case running time of an algorithm.

<youtube-video id="g2o22C3CRfU?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>

## Recursion

A Rescursive function is a function that calls itself inside of its own definition.
This video will help you understand functions and how to use them.

<youtube-video id="rf60MejMz3E?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>
