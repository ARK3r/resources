---
title: Courses
description: We're compiling a library of useful computer science courses from around the Internet to help you on your software journey!
position: 500
category: Computer Science
---

## Free Courses

### PBS Crash Course Computer Science

![PBS Crash Course Computer Science Preview](https://img.youtube.com/vi/tpIctyqH29Q/maxresdefault.jpg)

Crash Course Computer Science by PBS is our most recommended learning playlist for people of all levels after basic schooling.

Hosted by [Carrie Anne Philbin](https://www.geekgurldiaries.co.uk/), this course consists of 40 entertaining and educational videos based on introductory college-level material as well as the AP Computer Science Principles guidelines.

#### By the end of this course, you will be able to:

- Outline the history of computers and the design decisions that gave us modern computers
- Describe the basic elements of programming and software
- Identify the basic components of computer hardware and what they do
- Describe how computers are used and how that has evolved over time
- Appreciate how far computers have come and how far they might take us

<cta-button  link="https://youtube.com/playlist?list=PL8dPuuaLjXtNlUrzyH5r6jN9ulIgZBpdo" text="View Playlist!" > </cta-button>

### Computer Science Concepts by FreeCodeCamp

![Computer Science Concepts by FreeCodeCamp Preview](https://img.youtube.com/vi/zOjov-2OZ0E/maxresdefault.jpg)

This free YouTube playlist by FreeCodeCamp has full-length courses and short videos that cover many essential computer science concepts such as:

- Computer Science Terminology
- Data Structures
- Software Engineering
- Version Control with Git

<cta-button  link="https://youtube.com/playlist?list=PLWKjhJtqVAbn5emQ3RRG8gEBqkhf_5vxD" text="View Playlist!" > </cta-button>

### The Open Source Computer Science Degree

This is a curated list of free courses from reputable universities like MIT, Stanford, and Princeton that satisfy the same requirements as an undergraduate Computer Science degree, minus general education.

<cta-button text="Learn More" link="https://github.com/ForrestKnight/open-source-cs"></cta-button>

## University Courses

### Harvard CS50 2020: Intro to Computer Science

CS50 is Harvard University's introduction to the intellectual enterprises of computer science and the art of programming.

CS50 is an entry-level university course that teaches students how to think algorithmically and solve problems efficiently. It is suitable for advanced high school students or university students of all backgrounds.

Topics include abstraction, algorithms, data structures, encapsulation, resource management, security, software engineering, and web development.

<youtube-video id="YoXxevp1WRQ?list=PLhQjrBD2T382_R182iC2gNZI9HzWFMC_8"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PLoCMsyE1cvdWiqgyzwAz_uGLSHsuYZlMX"></cta-button>


### Introduction to Computer Science and Programming in Python (MIT)

This course by MIT is intended for students with little or no programming experience. It aims to provide students with an understanding of the role computation can play in solving problems and to help students, regardless of their major, feel justifiably confident of their ability to write small programs that allow them to accomplish useful goals. The class uses the Python 3.5 programming language.

<youtube-video id="nykOeWgQcHM?list=PLUl4u3cNGP63WbdFxL8giv4yhgdMGaZNA"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PLUl4u3cNGP63WbdFxL8giv4yhgdMGaZNA"></cta-button>


### Computer System Engineering (MIT)

This course covers topics on the engineering of computer software and hardware systems: techniques for controlling complexity; strong modularity using client-server design, virtual memory, and threads; networks; atomicity and coordination of parallel activities; recovery and reliability; privacy, security, and encryption; and impact of computer systems on society. Case studies of working systems and readings from the current literature provide comparisons and contrasts. Two design projects are required, and students engage in extensive written communication exercises

<youtube-video id="zm2VP0kHl1M?list=PL6535748F59DCA484"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PL6535748F59DCA484"></cta-button>

### Computer Science for Business Professionals (Harvard)

This course is a variant of Harvard College's introduction to computer science, designed especially for business professionals. It empowers students to make technological decisions even if not technologists themselves.

Topics include cloud computing, networking, privacy, scalability, security, and more, with an emphasis on web and mobile technologies.Students emerge from this course with firsthand appreciation of how it all works and all the more confident in the factors that should guide their decision-making.

This course is designed for managers, product managers, founders, and decision-makers more generally.

<youtube-video id="Q2f9h_-_Fv4?list=PLhQjrBD2T381YHS5L3gkwPbUGiI0foXuc"></youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PLhQjrBD2T381YHS5L3gkwPbUGiI0foXuc"></cta-button>
