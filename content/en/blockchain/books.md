---
title: Books
description: We're compiling useful blockchain books around the internet to help you on your software journey!
position: 9501
category: Blockhain
---

## Blockhain

### Bitcoin and Cryptocurrency Technologies 

<book-cover link="https://d28rh4a8wq0iu5.cloudfront.net/bitcointech/readings/princeton_bitcoin_book.pdf" img-src="https://i.imgur.com/ov1xiOL.jpg" alt="Book cover for Bitcoin and Cryptocurrency Technologies "> </book-cover>

Bitcoin and Cryptocurrency Technologies provides a comprehensive introduction to the revolutionary yet often misunderstood new technologies of digital currency. Whether you are a student, software developer, tech entrepreneur, or researcher in computer science, this authoritative and self-contained book tells you everything you need to know about the new global money for the Internet age.

<cta-button text="Video Lectures" link="http://bitcoinbook.cs.princeton.edu/"></cta-button>

<cta-button text="Download PDF" link="https://d28rh4a8wq0iu5.cloudfront.net/bitcointech/readings/princeton_bitcoin_book.pdf"></cta-button>


### Grokking Bitcoin

<book-cover link="https://rosenbaum.se/book/" img-src="https://i.imgur.com/cdONeq3.png" alt="Book cover for Grokking Bitcoin"> </book-cover>

The primary goal of this book is for you to be able to decide for yourself whether you trust Bitcoin. On the way to that goal, you’ll learn a number of Bitcoin concepts—such as digital signatures, proof of work, and peer-to-peer networks—on a pretty deep level.

This book is intended for technically interested people who want to understand Bitcoin on a deep technical level. The book doesn’t require any programming skills, but a basic understanding of some technical concepts is beneficial—for example, databases, computer networks, computer programs, and web servers. A little math background can be useful too, but it’s certainly not required.

<cta-button text="Complete Book" link="https://rosenbaum.se/book/"></cta-button>


### IBM Blockchain: The Founder’s Handbook - An introduction to building a blockchain solution

<book-cover link="https://www.ibm.com/downloads/cas/GZPPMWM5" img-src="https://i.imgur.com/obKJrcL.png" alt="Book cover for IBM Blockchain: The Founder’s Handbook - An introduction to building a blockchain solution"> </book-cover>

IBM Blockchain is more than technology. It’s a movement to help you redefine your most important business relationships through trust, transparency and newfound collaboration.

<cta-button text="Download PDF" link="https://www.ibm.com/downloads/cas/GZPPMWM5"></cta-button>

</br>

<cta-button text="More Books On Blockchain" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#blockchain"></cta-button>