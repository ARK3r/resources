---
title: Books
description: We're compiling a library of useful Algorithms and Data Structures books from around the Internet to help you on your software journey!
position: 12001
category: Algorithms and Data Structures
---

## Algorithms and Data Structures

### Algorithm Design 

<book-cover link="https://archive.org/details/AlgorithmDesign1stEditionByJonKleinbergAndEvaTardos2005PDF" img-src="https://i.imgur.com/oYJReTK.jpg" alt="Book cover for Algorithm Design"> </book-cover>

Algorithm Design introduces algorithms by looking at the real-world problems that motivate them. The book teaches students a range of design and analysis techniques for problems that arise in computing applications. The text encourages an understanding of the algorithm design process and an appreciation of the role of algorithms in the broader field of computer science.

<cta-button text="Complete Book" link="https://archive.org/details/AlgorithmDesign1stEditionByJonKleinbergAndEvaTardos2005PDF"></cta-button>


### Algorithms and Complexity

<book-cover link="https://www2.math.upenn.edu/~wilf/AlgoComp.pdf" img-src="https://i.imgur.com/40nht0h.jpg" alt="Book cover for Algorithms and Complexity "> </book-cover>

This book is an introductory textbook on the design and analysis of algorithms. The author uses a careful selection of a few topics to illustrate the tools for algorithm analysis. Recursive algorithms are illustrated by Quicksort, FFT, fast matrix multiplications, and others. Algorithms associated with the network flow problem are fundamental in many areas of graph connectivity, matching theory, etc. Algorithms in number theory are discussed with some applications to public key encryption. This second edition will differ from the present edition mainly in that solutions to most of the exercises will be included.

<cta-button text="Complete Book" link="https://www2.math.upenn.edu/~wilf/AlgoComp.pdf"></cta-button>


### Binary Trees

<book-cover link="http://cslibrary.stanford.edu/110/BinaryTrees.pdf" img-src="https://i.imgur.com/BD7oZ0o.png" alt="Book cover for Binary Trees"> </book-cover>

This article introduces the basic concepts of binary trees, and then works through a series of practice problems with
solution code in C/C++ and Java. Binary trees have an elegant recursive pointer structure, so they are a good way to
learn recursive pointer algorithms. 

<cta-button text="Complete Book" link="http://cslibrary.stanford.edu/110/BinaryTrees.pdf"></cta-button>

### The Design of Approximation Algorithms 

<book-cover link="http://www.designofapproxalgs.com/book.pdf" img-src="https://i.imgur.com/vJVu984.png" alt="Book cover for The Design of Approximation Algorithms "> </book-cover>

This book shows how to design approximation algorithms: efficient algorithms that find provably near-optimal solutions. The book is organized around central algorithmic techniques for designing approximation algorithms, including greedy and local search algorithms, dynamic programming, linear and semidefinite programming, and randomization.

Each chapter in the first part of the book is devoted to a single algorithmic technique, which is then applied to several different problems. The second part revisits the techniques but offers more sophisticated treatments of them. The book also covers methods for proving that optimization problems are hard to approximate. Designed as a textbook for graduate-level algorithms courses, the book will also serve as a reference for researchers interested in the heuristic solution of discrete optimization problems.

<cta-button text="Complete Book" link="http://www.designofapproxalgs.com/book.pdf"></cta-button>

</br>

<cta-button text="More Books On Algorithms and Data Structures" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#algorithms--data-structures"></cta-button>