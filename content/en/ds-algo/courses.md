---
title: Courses
description: We're compiling a library of useful algorithms and data structures courses from around the Internet to help you on your software journey!
position: 12000
category: Algorithms and Data Structures
---

## Introduction to Algorithms (MIT)

This course is an introduction to mathematical modeling of computational problems, as well as common algorithms, algorithmic paradigms, and data structures used to solve these problems. It emphasizes the relationship between algorithms and programming and introduces basic performance measures and analysis techniques for these problems.

<youtube-video id="ZA-tUyM_y7s?list=PLUl4u3cNGP63EdVPNLG3ToM6LaEUuStEY"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PLUl4u3cNGP63EdVPNLG3ToM6LaEUuStEY"></cta-button>

## Advanced Algorithms (Harvard)

This is an advanced course in algorithm design, and topics that are covered include the word RAM model, data structures, amortization, online algorithms, linear programming, semidefinite programming, approximation algorithms, hashing, randomized algorithms, fast exponential time algorithms, graph algorithms, and computational geometry.

<youtube-video id="0JUN9aDxVmI?list=PL2SOU6wwxB0uP4rJgf5ayhHWgw7akUWSf"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PL2SOU6wwxB0uP4rJgf5ayhHWgw7akUWSf"></cta-button>

## Design and Analysis of Algorithms (MIT)

This is an intermediate algorithms course with an emphasis on teaching techniques for the design and analysis of efficient algorithms, emphasizing methods of application. Topics include divide-and-conquer, randomization, dynamic programming, greedy algorithms, incremental improvement, complexity, and cryptography.

<youtube-video id="2P-yW7LQr08?list=PLUl4u3cNGP6317WaSNfmCvGym2ucw3oGp"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PLUl4u3cNGP6317WaSNfmCvGym2ucw3oGp"></cta-button>