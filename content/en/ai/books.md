---
title: Books
description: We're compiling a library of useful Artificial Intelligence books from around the Internet to help you on your software journey!
position: 5506
category: Artificial Intelligence
---

## Artificial Intelligence

### Artificial Intelligence: Foundations of Computational Agents

<book-cover link="https://artint.info/aifca1e.html" img-src="https://i.imgur.com/agMof9g.jpg" alt="Book cover for Artificial Intelligence: Foundations of Computational Agents"> </book-cover>

Foundations of Computational Agents is a book about the science of artificial intelligence (AI). The book works as an introductory text on artificial intelligence for advanced undergraduate or graduate students in computer science or related disciplines such as computer engineering, philosophy, cognitive science, or psychology.

It will appeal more to the technically minded; parts are technically challenging, focusing on learning by doing: designing, building, and implementing systems.

<cta-button text="Complete Book" link="https://artint.info/aifca1e.html"></cta-button>


### Artificial Intelligence for a Better Future: An Ecosystem Perspective on the Ethics of AI and Emerging Digital Technologies

<book-cover link="https://link.springer.com/content/pdf/10.1007/978-3-030-69978-9.pdf" img-src="https://i.imgur.com/r1jLeHy.jpg" alt="Book cover for Artificial Intelligence for a Better Future: An Ecosystem Perspective on the Ethics of AI and Emerging Digital Technologies"> </book-cover>

This open access book proposes a novel approach to Artificial Intelligence (AI) ethics.
It presents an innovative answer to the question "how can we benefit from AI while addressing its ethical problems?" by presenting a different perspective on AI and its ethical consequences  and suggests practical measures to ensure that AI is used to make the world a better place.

<cta-button text="Download PDF" link="https://link.springer.com/content/pdf/10.1007/978-3-030-69978-9.pdf"></cta-button>


### On the Path to AI: Law’s prophecies and the conceptual foundations of the machine learning age 

<book-cover link="https://link.springer.com/content/pdf/10.1007/978-3-030-43582-0.pdf" img-src="https://i.imgur.com/az6a4qt.png" alt="Book cover for On the Path to AI: Law’s prophecies and the conceptual foundations of the machine learning age"> </book-cover>

This open access book explores machine learning and its impact on how we make sense of the world.
It does so by bringing together two ‘revolutions’ in a surprising analogy: the revolution of machine learning, which has placed computing on the path to artificial intelligence, and the revolution in thinking about the law that was spurred by Oliver Wendell Holmes Jr in the last two decades of the 19th century. Holmes reconceived law as prophecy based on experience, prefiguring the buzzwords of the machine learning age—prediction based on datasets.

<cta-button text="Download PDF" link="https://link.springer.com/content/pdf/10.1007/978-3-030-43582-0.pdf"></cta-button>

</br>

<cta-button text="More Books On Machine Learning" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#artificial-intelligence"></cta-button>


## R

### Advanced R Programming 

<book-cover link="http://adv-r.had.co.nz/" img-src="https://i.imgur.com/6yAQ4xs.jpg" alt="Book cover for Advanced R Programming "> </book-cover>

This book is designed primarily for R users who want to improve their programming skills and understanding of the language. It should also be useful for programmers coming to R from other languages, as it explains some of R’s quirks and shows how some parts that seem horrible do have a positive side

<cta-button text="Complete Book" link="http://adv-r.had.co.nz/"></cta-button>


### Data Analysis and Prediction Algorithms with R

<book-cover link="https://rafalab.github.io/dsbook/index.html#preface" img-src="https://i.imgur.com/I143iCQ.png" alt="Book cover for Data Analysis and Prediction Algorithms with R"> </book-cover>

The demand for skilled data science practitioners in industry, academia, and government is rapidly growing. This book introduces concepts from probability, statistical inference, linear regression and machine learning and R programming skills. Throughout the book we demonstrate how these can help you tackle real-world data analysis challenges.

<cta-button text="Complete Book" link="https://rafalab.github.io/dsbook/index.html#preface"></cta-button>


### The R Inferno

<book-cover link="https://www.burns-stat.com/pages/Tutor/R_inferno.pdf" img-src="https://i.imgur.com/Cm8qPjE.jpg" alt="Book cover for The R Inferno"> </book-cover>

An essential guide to the trouble spots and oddities of R. In spite of the quirks exposed here, R is the best computing environment for most data analysis tasks.

R is free, open-source, and has thousands of contributed packages. 

<cta-button text="Download PDF" link="https://www.burns-stat.com/pages/Tutor/R_inferno.pdf"></cta-button>

</br>

<cta-button text="More Books On R Programming" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#r"></cta-button>

<cta-button text="More Resources On R Programming" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#r"></cta-button>

## Python

### A Practical Introduction to Python Programming 

<book-cover link="https://www.brianheinold.net/python/A_Practical_Introduction_to_Python_Programming_Heinold.pdf" img-src="https://i.imgur.com/grYl3mz.png" alt="Book cover for A Practical Introduction to Python Programming "> </book-cover>

This book is specifically designed to be used in an introductory programming course, but it is also useful for those with prior programming experience looking to learn Python.

<cta-button text="Download PDF" link="https://www.brianheinold.net/python/A_Practical_Introduction_to_Python_Programming_Heinold.pdf"></cta-button>


### Full Stack Python

<book-cover link="https://www.fullstackpython.com/" img-src="https://i.imgur.com/2sItjcV.jpg" alt="Book cover for Full Stack Python"> </book-cover>

Full Stack Python explains important Python concepts in plain language terms without assuming that you already know much about web development, deployments or running an application. Each chapter focuses on a large subject area, such as data or development environments, then digs into concepts and implementations that you should know to make the right choices for what to use when building your projects.

Think of Full Stack Python as your high-level guide to Python development so that you can understand what you do not know and gain a map for what to research next.

<cta-button text="Complete Book" link="https://www.fullstackpython.com/"></cta-button>


### Invent Your Own Computer Games With Python

<book-cover link="https://inventwithpython.com/invent4thed/" img-src="https://i.imgur.com/xQ2Wsea.png" alt="Book cover for Invent Your Own Computer Games With Python"> </book-cover>

Invent Your Own Computer Games with Python teaches you how to program in the Python language. Each chapter gives you the complete source code for a new game, and then teaches the programming concepts from the examples. Games include Guess the Number, Hangman, Tic Tac Toe, and Reversi. This book also has an introduction to making games with 2D graphics using the Pygame framework.

<cta-button text="Complete Book" link="https://inventwithpython.com/invent4thed/"></cta-button>

</br>

<cta-button text="More Books On Python" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#python"></cta-button>

<cta-button text="More Resources On Python" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#python"></cta-button>

