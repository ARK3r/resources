---
title: Books
description: We're compiling a library of useful computer vision books from around the Internet to help you on your software journey!
position: 7002
category: Computer Vision
---

## Computer Vision

### Computer Vision - Dana Ballard, Chris Brown

<book-cover link="https://homepages.inf.ed.ac.uk/rbf/BOOKS/BANDB/Ballard__D._and_Brown__C._M.__1982__Computer_Vision.pdf" img-src="https://i.imgur.com/asZfXQt.png" alt="Book cover for Computer Vision"> </book-cover>

Computer vision is the construction of explicit, meaningful descriptions of physical objects from images. Image understanding is very different from image processing, which studies image-to-image transformations, not explicit description building. Descriptions are a prerequisite for recognizing, manipulating, and thinking about objects.

Parts of the book assume some mathematical and computing background (calculus, linear algebra, data structures, numerical methods). However, throughout the book mathematical rigor takes a backseat to concepts. The intent is to transmit a set of ideas about a new field to the widest possible audience.

<cta-button text="Download PDF" link="https://homepages.inf.ed.ac.uk/rbf/BOOKS/BANDB/Ballard__D._and_Brown__C._M.__1982__Computer_Vision.pdf"></cta-button>


### Computer Vision: Algorithms and Applications 

<book-cover link="http://szeliski.org/Book/" img-src="https://i.imgur.com/uuWZMvn.jpg" alt="Book cover for Computer Vision: Algorithms and Applications"> </book-cover>

This book explores the variety of techniques used to analyze and interpret images. It also describes challenging real-world applications where vision is being successfully used, both in specialized applications such as image search and autonomous navigation, as well as for fun, consumer-level tasks that students can apply to their own personal photos and videos.

<cta-button text="Download PDF" link="http://szeliski.org/Book/"></cta-button>


### Programming Computer Vision with Python

<book-cover link="http://programmingcomputervision.com/downloads/ProgrammingComputerVision_CCdraft.pdf" img-src="https://i.imgur.com/7TO30JO.jpg" alt="Book cover for Programming Computer Vision with Python"> </book-cover>

If you want a basic understanding of computer vision’s underlying theory and algorithms, this hands-on introduction is the ideal place to start. You’ll learn techniques for object recognition, 3D reconstruction, stereo imaging, augmented reality, and other computer vision applications as you follow clear examples written in Python.

Programming Computer Vision with Python explains computer vision in broad terms that won’t bog you down in theory. You get complete code samples with explanations on how to reproduce and build upon each example, along with exercises to help you apply what you’ve learned. This book is ideal for students, researchers, and enthusiasts with basic programming and standard mathematical skills.

<cta-button text="Download PDF" link="http://programmingcomputervision.com/downloads/ProgrammingComputerVision_CCdraft.pdf"></cta-button>

</br>

<cta-button text="More Books On Computer Vision" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#computer-vision"></cta-button>