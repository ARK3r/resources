---
title: Videos
description: We're compiling a library of useful computer vision resources from around the Internet to help you on your software journey!
position: 7000
category: Computer Vision
---

## How Blurs & Filters Work

Image filters make most people think of Instagram or Camera Phone apps, but what's really going on at the pixel level?

In this video, Image Analyst [Dr Mike Pound](https://twitter.com/_mikepound) explains some of the most common filters.

<youtube-video id="C_zFhWdM4ic?list=PLzH6n4zXuckoRdljSlM2k35BufTYXNNeF"></youtube-video>

## Finding the Edges

Our eyes can spot edges with no problems, but how do computers determine what's an edge and what's not?

In this video, Image Analyst [Dr Mike Pound](https://twitter.com/_mikepound) explains the Sobel Edge detector.

<youtube-video id="uihBwtPIBxM?list=PLzH6n4zXuckoRdljSlM2k35BufTYXNNeF"></youtube-video>

## Canny Edge Detector

This video will take edges one step further with Hysteresis Thresholding - The Canny Operator explained by Image Analyst [Dr Mike Pound](https://twitter.com/_mikepound)

<youtube-video id="sRFM5IEqR2w?list=PLzH6n4zXuckoRdljSlM2k35BufTYXNNeF"></youtube-video>

## Resizing Images

In this video you will learn about the Nearest Neighbour and BiLinear Resize concepts, explained by [Dr Mike Pound](https://twitter.com/_mikepound)

<youtube-video id="AqscP7rc8_M?list=PLzH6n4zXuckoRdljSlM2k35BufTYXNNeF"></youtube-video>

## Deep Learned Super-Sampling (DLSS)

Can deep learning improve your gaming experience? We have no idea but we know how it works.

In this video [Dr Mike Pound](https://twitter.com/_mikepound) talks about Deep Learned Super Sampling.

<youtube-video id="_DPRt3AcUEY?list=PLzH6n4zXuckoRdljSlM2k35BufTYXNNeF"></youtube-video>

## Video Game & Complex Bokeh Blurs

How do Madden, FIFA, PGA Tour get that lovely shallow depth of field in real time?

In this video [Dr Mike Pound](https://twitter.com/_mikepound) explains how Complex Gaussian Blurs can be separable.

<youtube-video id="vNG3ZAd8wCc?list=PLzH6n4zXuckoRdljSlM2k35BufTYXNNeF"></youtube-video>

## JPEG 'files' & Colour

In this video you will learn how JPEG isn't a file format. Image Analyst [Dr Mike Pound](https://twitter.com/_mikepound) explains why not in our first in a series about how JPEG works.

<youtube-video id="n_uNPbdenRs?list=PLzH6n4zXuckoRdljSlM2k35BufTYXNNeF"></youtube-video>

## The Problem with JPEG

In this video you will learn to never use JPEG with text. But why? Image Analyst [Dr Mike Pound](https://twitter.com/_mikepound) explains what goes wrong when JPEG tries to compress text.

<youtube-video id="yBX8GFqt6GA?list=PLzH6n4zXuckoRdljSlM2k35BufTYXNNeF"></youtube-video>

## Optical Flow

In this video you will learn pixel level movement in images - [Dr Andy French](https://scholar.google.co.uk/citations?user=SESz4-UAAAAJ&hl=en) takes you through the idea of Optic or Optical Flow.

<youtube-video id="5AUypv5BNbI?list=PLzH6n4zXuckoRdljSlM2k35BufTYXNNeF"></youtube-video>
