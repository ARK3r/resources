---
title: Books
description: We're compiling a library of useful TypeScript books from around the Internet to help you on your software journey!
position: 18501
category: TypeScript
---

## TypeScript

### Tackling TypeScript: Upgrading from JavaScript 
<book-cover link="https://exploringjs.com/tackling-ts/toc.html" img-src="https://i.imgur.com/fKXUhDP.jpg" alt="Book cover for Tackling TypeScript: Upgrading from JavaScript"> </book-cover>

This book consists of two parts:

- Part 1 is a quick start for TypeScript that teaches you the essentials quickly.
- Part 2 digs deeper into the language and covers many important topics in detail.

<cta-button text="Complete Book" link="https://exploringjs.com/tackling-ts/toc.html"></cta-button>


### TypeScript for C# Programmers

<book-cover link="https://www.infoq.com/minibooks/typescript-c-sharp-programmers/" img-src="https://i.imgur.com/ZOcAJGl.jpg" alt="Book cover for"> </book-cover>

Due to the popularity of the open web, JavaScript is becoming an essential language and since 2009 it has been running on servers too thanks to NodeJS. The problem is that due to JavaScript's dynamic type system, it is hard to create great tooling around the language such as sensible auto-completion, refactoring support, type-checking and modularisation.

TypeScript is an open source lanaguage from Microsoft that solves this problem by introducing an optional type system and class-based object-orientation, which make great tooling for large applications possible.
TypeScript let's you write JavaScript that is robust enough for the enterprise and that can run in any browser, on any host and on any operating system.

<cta-button text="Complete Book" link="https://www.infoq.com/minibooks/typescript-c-sharp-programmers/"></cta-button>


### Learn TypeScript in Y Minutes

[![Learn TypeScript in Y Minutes Preview](https://i.imgur.com/ijsGmDj.png)](https://learnxinyminutes.com/docs/typescript/)

TypeScript is a language that aims at easing development of large scale applications written in JavaScript. TypeScript adds common concepts such as classes, modules, interfaces, generics and (optional) static typing to JavaScript. It is a superset of JavaScript: all JavaScript code is valid TypeScript code so it can be added seamlessly to any project. The TypeScript compiler emits JavaScript.

<cta-button text="Complete Book" link="https://learnxinyminutes.com/docs/typescript/"></cta-button>

</br>

<cta-button text="More Books On TypeScript" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#typescript"></cta-button>